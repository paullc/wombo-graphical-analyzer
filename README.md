# Welcome to Wombo

Built on the JavaFX framework, Wombo is a graphical Java tool that performs combinatorial assessments of user-drawn directed and undirected graphs.

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/src/main/java/resources/icons/coralNodeStripped.png" width=250/> </div>

## Get Started With Wombo
Wombo can be used on any major platform, and the downloadable assets you need in order to use Wombo can be found in the [***releases***](https://gitlab.com/paullc/wombo-graphical-analyzer/-/releases) tab of this repository. Within each release are a few options.

#### Mac: 

 For MacOS, the *.pkg* and *.dmg* files will allow you to install Wombo to your computer as an application. The *WomboMacOS-1.0.zip* archive will also allow a user to use Wombo on a Mac, but it will not install it to your computer. The *zip* file must be extracted and the application will be found in the extracted folder. On Mac, extracting is as simple as double clicking on the *zip* file. The *.jar* file will also work on Mac (without installing it as an application) as long as you have the **Java SE Runtime Environment 8** or later.  Running Wombo from the *.jar* is as simple as clicking the *.jar*.

In any of these cases, your Mac will likely *block the application* from being run as it will be seen as foreign software which could harm your Mac. To run the application, you must choose to *"open anyway"* from your Mac's *"System Preferences -> Security and Privacy"* menu after it warns you that the software may be dangerous.


#### Windows:

For Windows, the *WomboWin-1.0.zip* archive will allow a user to run Wombo without installing it. Consequently, this version of Wombo can be transported or stored and run elsewhere since it is not tied to a specific instance of Windows. To access the application, the *.zip* file must be extracted, just like with Mac. On windows, this can be achieved by clicking on the *.zip* file and clicking the "Extract All" option in the File Explorer window. Within the extracted folder will be another folder called *"Wombo-1.0"*. Within that folder will be the double-clickable program called *Wombo.exe*. 

Windows can also use the *.jar* to run Wombo (without installing it) as long as you have the **Java SE Runtime Environment 8** or later. In the case of the *.jar*, running Wombo is as simple as double clicking the *.jar* file. 

Similarly to Mac, when attempting to run Wombo, your PC will likely inform you that it has prevented you from running the application for security reasons. On Windows this can be ignored by clicking on *"More info"* in the warning message window and then clicking *"Run anyway"*.


## How to Use Wombo


### Drawing a Graph

Wombo's behavior is built with "graph-theoretical" graphs in mind. This means graphs which, generally, consist of **vertices** connected by **edges**. 

Everything you need to draw an appropriate graph is available in the leftmost pane of Wombo's main window, called the *"Properties"* pane. To draw any of the features present in the properties pane, you must select the button representing the feature, and then some action must be performed in the "Graph" pane. 

#### Vertices
Wombo supports **2** kinds of vertices: **default** and **heuristic**. Default vertices are used for 6 out of the 7 total available algorithms. Heuristic vertices are used for the **A\* Shortest Path Algorithm** because they contain an additional measurement the algorithm uses. The only difference between heuristic and default vertices is that when a heuristic vertex is created, a numeric field must be entered.  This field represents a *value* that measures how *"good"* a choice a certain vertex would be for the A* algorithm when determining the shortest path between two points. 

To draw a vertex, you need to click on the vertex variant you wish to draw, and then simply click any number of times on the Graph pane. In the case of a heuristic vertex, after clicking on the graph pane, a dialogue will pop up where you can enter a *numeric* value for the vertex. After clicking *ok* or your *enter* key, the vertex will be created. Vertices will be automatically numbered starting from an index of **zero**. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/drawDefaultVertex.gif"/> </div>
<br/>
<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/drawHeuristicVertex.gif"/> </div>

#### Edges

Wombo supports **2**  kinds of edges: **directed** and **undirected**. All edges have a *"weight"* which is the cost of crossing the edge. This value could represent a real life metric like the distance along a road between two cities. Undirected edges represent the default edge type. Undirected edges can be traveled in both directions. That is, an edge drawn between a vertex A and a vertex B can be traveled from *A to B* or from *B to A*. 

On the other hand, a directed edge can only be traveled in one direction. A directed edge between a vertex A and a vertex B can **only** be traveled in the direction of the edge. For example, an ege drawn from *A to B* that points *towards B* can only be traveled from *A to B* and **not** from *B to A*.  In Wombo, the direction of an edge is represented using a small circle that is closest to the vertex the edge is pointing to. 

Since Wombo does not support multi-graphs, there is a maximum of **one undirected edge** or **two directed edges** (pointing in opposite directions) that can be drawn between any two vertices.

To draw any edge, you must select the *undirected* or *directed* edge options from the Properties pane. Then, you must click on two vertices. The edge will then appear between the two selected vertices. For directed edges, the direction of the edge depends on the order in which you select the two vertices. For example, to draw a directed edge *from vertex A to vertex B*, click the *directed* option, then click on vertex A, and *then* click on vertex B.  

All edges drawn will have their associated weights visually attached to them. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/drawUndirectedEdge.gif"/> </div>
<br/>
<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/drawDirectedEdge.gif"/> </div>

### Manipulating a Graph
After a graph has been drawn, Wombo offers a few options for manipulation of the graph.

#### Move
The *Move* option allows you to drag vertices around in the Graph pane. So, if you want to reconfigure the physical look of your graph, you can click the *move* option and then drag any vertex to a new position.  Any edges connected to that vertex will follow the vertex to its new position. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/moveVertices.gif"/> </div>


#### Delete
The *Delete* option allows you to delete individual features of the graph. If you make a mistake or simply want to change up the graph, you can delete individual vertices or edge by clicking the *move* option and then clicking on either an *edge* or a *vertex*. Clicking on an edge will simply remove that edge from the graph. Clicking on a vertex will delete the vertex in addition to any edges that were connected to it. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/deleteObjects.gif"/> </div>


#### New Graph
The *New Graph* option allows you to completely delete the currently displayed graph and start fresh. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/newGraph.gif"/> </div>


#### Resetting the Graph Style
After running a certain algorithm, the graph will acquire a style representing the output of that algorithm. To reset the *style* of the graph, you can double click the Graph pane's background. The style of the graph's vertices and edges will be changed back to the default. This does **not** change *any* of the properties of the graph other than the colors. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/resetStyle.gif"/> </div>



### Viewing Details About the Graph
Details about the graph you are drawing can be seen in the rightmost *Details* pane of Wombo's main window. These values will automatically update as you draw your graph and show:

 - Number of vertices
 - Number of edges
 - Whether the graph is connected
 - What the connection strength is *(if the graph has directed edges)*

## Algorithms

You can rename the current file by clicking the file name in the navigation bar or by clicking the **Rename** button in the file explorer.

### Selecting an Algorithm
To select an algorithm to run on a drawn graph, simply click on the "Choose Algorithm" drop-down selection menu in the Properties pane. This will give you **7** options from which to choose. To run one of them, simply click that option and the algorithm will be automatically run. For path-finding algorithms like Dijkstra's or A* additional actions like the selection of a start and end vertex are necessary. 

To **rerun** the same algorithm, click the "Rerun Algorithm" button located at the bottom of the "Details" pane. This will rerun the same algorithm you choose from the drop-down selector. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/rerunAlgorithm.gif"/> </div>


### Minimum Spanning Tree
This algorithm will generate a set of edges that connect all the vertices present in the graph such that the total cost of connecting all the vertices is minimized. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/minSpanTree.gif"/> </div>


### Hamiltonian Path
This algorithm will generate a path through the graph that visits *each* vertex only **once** and ends once all vertices have been included.

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/hamiltonianPath.gif"/> </div>


### Hamiltonian Cycle
This algorithm will generate a *cycle* through the graph that visits *each* vertex only **once**. Thus, the generated path will start and end at the same vertex.
 
 <div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/hamiltonianCycle.gif"/> </div>


### Dijkstra's Shortest Path
This algorithm will find the shortest path from the first chosen vertex to the second chosen vertex. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/dijkstra.gif"/> </div>


### A* Shortest Path
This algorithm will find the shortest path from the first chosen vertex to the second chosen vertex. However, *unlike* Dijkstra's algorithm,  this algorithm will choose its next vertices more intelligently assuming the heuristic values of the graph's vertices are accurate. Given more complex graphs where the heuristic values of their vertices are accurate, this algorithm will perform more efficiently than Dijkstra's, but will result in the same path. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/aStar.gif"/> </div>


### Graph Coloring
This algorithm will color each vertex such that no adjacent vertex (connected by an edge) has the same color. Thus, this algorithm will determine an approximation for the Chromatic Number of the drawn graph. 

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/chromatic.gif"/> </div>


### Travelling Salesman Tour
This algorithm will attempt to find a Hamiltonian cycle such that the found cycle is the **minimum** Hamiltonian cycle in the graph.

*NOTE:* Because this algorithm is of the **NP: Hard** category, there exists no solution that is *both optimal and efficient*. I opted to provide the most optimal solution, which required a brute-force algorithm. Consequently, as the complexity of the provided graph increases, the time required to compute the result increases dramatically. Graphs with many vertices can take longs periods of time to find a solution and can freeze the program. In the future I may implement an approximation algorithm that kicks in after a certain number of nodes has been reached.

<div align="center"> <img src="https://gitlab.com/paullc/wombo-graphical-analyzer/-/raw/master/gifs/salesman.gif"/> </div>
