package main.java;

import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        stage.setTitle("Wombo");
        stage.getIcons().add(new Image(App.class.getResource("resources/icons/coralNodeStripped.png").toExternalForm()));
        stage.setMinHeight(600);
        stage.setMinWidth(900);


        Parent launchRoot = FXMLLoader.load(getClass().getResource("resources/launch.fxml"));
        Parent mainWindowRoot = FXMLLoader.load(getClass().getResource("resources/mainWindow.fxml"));
        Scene launchScene = new Scene(launchRoot);

        stage.setScene(launchScene);
        stage.show();

        PauseTransition delay = new PauseTransition(Duration.seconds(2.5));
        delay.setOnFinished(event -> stage.setScene(new Scene(mainWindowRoot,
                launchScene.getWindow().getWidth(),
                launchScene.getWindow().getHeight())));
        delay.play();

    }

    public static void main(String[] args) {
        launch();
    }

}
