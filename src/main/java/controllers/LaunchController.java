package main.java.controllers;

import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@SuppressWarnings("DuplicatedCode")
public class LaunchController {


    public GridPane gridPane;

    public void goToRepoLink() throws URISyntaxException, IOException {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            Desktop.getDesktop().browse(new URI("https://gitlab.com/paullc/wombo-graphical-analyzer"));
        }
    }

    public void goToIconCredit() throws URISyntaxException, IOException {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            Desktop.getDesktop().browse(new URI("https://www.flaticon.com/authors/freepik"));
        }
    }

    public void aboutPane() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(this.gridPane.getScene().getWindow());
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setGraphic(null);

        DialogPane pane = alert.getDialogPane();
        pane.setId("aboutPane");
        pane.getStylesheets().clear();
        String css = getClass().getResource("dialog.css").toExternalForm();
        pane.getStylesheets().add(css);
        pane.getStyleClass().add(css);

        alert.setTitle("About");
        alert.setHeaderText("About the Developer");
        alert.setContentText("Hello! I'm Pau Lleonart Calvo, the developer behind Wombo. I'm a rising senior pursuing " +
                "a B.S. in Computer Science at Virginia Tech.\n\nI developed Wombo in the " +
                "summer of 2020 over the span of about two weeks. I had just finished my Applied Combinatorics " +
                "class and imagined that a tool like Wombo would have made my life a whole lot easier. So, I drew " +
                "out plans and developed the program that you are using right now. If you have any suggestions or " +
                "questions, I can be reached at the following address: \n\n" +
                "plleonartcalvo@gmail.com\n\n" +
                "I hope this tool serves you well. Enjoy!");

        alert.show();
    }
}
