package main.java.controllers;

import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import main.java.*;
import main.java.algorithms.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

/**
 *
 */
@SuppressWarnings("DuplicatedCode")
public class MainWindowController implements Initializable {

    private Graph graph;
    private Vertex firstVertex;
    private StackPane firstCircle;
    private Vertex secondVertex;
    private StackPane secondCircle;
    private boolean clickedVertex;
    private boolean isConnected;

    public AnchorPane propertiesPane;
//    public AnchorPane graphOverlay;
    public AnchorPane detailsPane;
    public SplitPane splitPane;
    public AnchorPane graphPane;

    public Button directedButton;
    public Button undirectedButton;
    public Button defaultVertexButton;
    public Button heuristicVertexButton;
    public Button deleteObjectButton;
    public Button newGraphButton;
    public Button moveButton;
    public Button rerunButton;


    public ComboBox<String> algorithmComboBox;

    public Label connectedText;
    public Label strengthText;
    public Label leftStatus;

    public Label numVerticesText;
    public Label numEdgesText;

    private CursorState cursorState;
    private SelectionState selectionState;
    private CursorState previousState;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.splitPane.getStylesheets().clear();
        String css = getClass().getResource("splitPane.css").toExternalForm();
        this.splitPane.getStylesheets().add(css);
        this.splitPane.getStyleClass().add(css);
        this.splitPane.setDividerPosition(0, 0.2600);
        this.splitPane.setDividerPosition(1, 0.8612);
        this.numVerticesText.setText("0");
        this.numEdgesText.setText("0");
        this.connectedText.setText("FALSE");
    }


    /**
     *
     */
    public MainWindowController() {
        this.graph = new Graph();
        this.cursorState = CursorState.NONE;
        this.selectionState = SelectionState.ZERO;
        this.clickedVertex = false;
    }


    /**
     * @throws URISyntaxException Thrown if the URI was invalid or encountered an error while parsing.
     * @throws IOException Thrown if the desktop's browser encountered an error when attempting to be opened to the
     * provided link.
     */
    public void goToRepoLink() throws URISyntaxException, IOException {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            Desktop.getDesktop().browse(new URI("https://gitlab.com/paullc/wombo-graphical-analyzer"));
        }
    }


    public void aboutPane() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(this.graphPane.getScene().getWindow());
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setGraphic(null);

        DialogPane pane = alert.getDialogPane();
        pane.setId("aboutPane");
        pane.getStylesheets().clear();
        String css = getClass().getResource("dialog.css").toExternalForm();
        pane.getStylesheets().add(css);
        pane.getStyleClass().add(css);

        alert.setTitle("About");
        alert.setHeaderText("About the Developer");
        alert.setContentText("Hello! I'm Pau Lleonart Calvo, the developer behind Wombo. I'm a rising senior pursuing " +
                "a B.S. in Computer Science at Virginia Tech.\n\nI developed Wombo in the " +
                "summer of 2020 over the span of about two weeks. I had just finished my Applied Combinatorics " +
                "class and imagined that a tool like Wombo would have made my life a whole lot easier. So, I drew " +
                "out plans and developed the program that you are using right now. If you have any suggestions or " +
                "questions, I can be reached at the following address: \n\n" +
                "plleonartcalvo@gmail.com\n\n" +
                "I hope this tool serves you well. Enjoy!");

        alert.show();
    }



    /**
     */
    @SuppressWarnings("unused")
    public void openFile() {
        FileChooser fileChooser = new FileChooser();

        File selectedFIle = fileChooser.showOpenDialog(new Stage());
    }

    /**
     * @param actionEvent The event fired by the clicking of a Property button in the GUI.
     */
    public void clickedProperty(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        if (!button.getId().contentEquals("newGraphButton")) {
            for (Node node : this.propertiesPane.getChildren()) {
                if (node instanceof Button) {
                    node.setOpacity(1);
                    ((Button) node).setTextFill(new Color(1, 1, 1, 1));
                }
            }
        }

        switch (button.getId()) {
            case ("defaultVertexButton"): {
                this.defaultVertexButton.setTextFill(new Color(0.5, 0.5, 0.5, 1));
                this.cursorState = CursorState.DEFAULT;
                this.selectionState = SelectionState.ZERO;
                this.leftStatus.setText("Recent Action: Clicked Add Default Vertex");
                break;
            }
            case ("heuristicVertexButton"): {
                this.heuristicVertexButton.setTextFill(new Color(0.5, 0.5, 0.5, 1));
                this.cursorState = CursorState.HEURISTIC;
                this.selectionState = SelectionState.ZERO;
                this.leftStatus.setText("Recent Action: Clicked Add Heuristic Vertex");
                break;
            }
            case ("undirectedButton"): {
                this.undirectedButton.setTextFill(new Color(0.5, 0.5, 0.5, 1));
                this.cursorState = CursorState.UNDIRECTED;
                this.selectionState = SelectionState.ZERO;
                this.leftStatus.setText("Recent Action: Clicked Add Undirected Edge");
                break;
            }
            case ("directedButton"): {
                this.directedButton.setTextFill(new Color(0.5, 0.5, 0.5, 1));
                this.cursorState = CursorState.DIRECTED;
                this.selectionState = SelectionState.ZERO;
                this.leftStatus.setText("Recent Action: Clicked Add Directed Edge");
                break;
            }
            case ("moveButton"): {
                this.moveButton.setTextFill(new Color(0.5, 0.5, 0.5, 1));
                this.cursorState = CursorState.MOVE;
                this.selectionState = SelectionState.ZERO;
                this.leftStatus.setText("Recent Action: Clicked Move Object Tool");
                break;
            }
            case ("deleteObjectButton"): {
                this.deleteObjectButton.setTextFill(new Color(0.5, 0.5, 0.5, 1));
                this.cursorState = CursorState.DELETE;
                this.selectionState = SelectionState.ZERO;
                this.leftStatus.setText("Recent Action: Clicked Delete Object Tool");
                break;
            }
            case ("newGraphButton"): {
                ArrayList<Node> delete = new ArrayList<>();
                for (Node node : this.graphPane.getChildren()) {
                    if (node instanceof StackPane) {
                        delete.add(node);
                    } else if (node instanceof Line) {
                        delete.add(node);
                        delete.add((Label) node.getProperties().get("label"));
                    } else if (node instanceof  Path) {
                        delete.add(node);
                        delete.add((Label) node.getProperties().get("label"));
                        delete.add((Circle) node.getProperties().get("directionCircle"));
                    }
                }
                this.graphPane.getChildren().removeAll(delete);
                this.graph = new Graph();
                this.leftStatus.setText("Recent Action: Generated New Graph");
                this.updateGraphInfo();
                break;
            }
        }
    }

    /**
     */
    public void clickedAlgorithm() {
        this.updateGraphInfo();
        String value = this.algorithmComboBox.getValue();

        if (value == null) {
            return;
        }
        this.leftStatus.setText("Recent Action: Chose Algorithm '" + this.algorithmComboBox.getValue() + "'");
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(this.graphPane.getScene().getWindow());
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setGraphic(null);

        DialogPane pane = alert.getDialogPane();
        pane.setId("algorithmInfo");
        pane.getStylesheets().clear();
        String css = getClass().getResource("dialog.css").toExternalForm();
        pane.getStylesheets().add(css);
        pane.getStyleClass().add(css);
        this.resetGraphLook();


        switch (value) {
            case ("Minimum Spanning Tree"): {
                if (!this.isConnected) {
                    alert.setTitle("Minimum Spanning Tree");
                    alert.setHeaderText("No Tree Found");
                    alert.setContentText("Graph must be connected for a Spanning Tree to be Possible");
                    alert.show();
                    break;
                }

                MinSpanTree minSpanTree = new MinSpanTree(this.graph);
                Graph tree = minSpanTree.getMinSpanningTree();

                alert.setTitle("Minimum Spanning Tree");
                alert.setHeaderText("Tree Found With Cost: " + minSpanTree.getCost());
                alert.setContentText(minSpanTree.getResultString());

                this.displayMinSpanTree(tree);
                alert.show();
                this.leftStatus.setText(minSpanTree.getExecutionTime());

                break;
            }
            case ("Find Hamiltonian Path"): {
                if (!this.isConnected) {
                    alert.setTitle("Hamiltonian Path");
                    alert.setHeaderText("No Hamiltonian Path Found");
                    alert.setContentText("Graph must be connected for a Hamiltonian Path to be Possible");
                    alert.show();
                    break;
                }
                Hamilton hamilton = new Hamilton(this.graph);
                ArrayList<Vertex> vertices = hamilton.getPath();
                if (vertices == null) {
                    alert.setTitle("Hamiltonian Path");
                    alert.setHeaderText("No Hamiltonian Path Found");
                    alert.setContentText("No path was found for this graph");
                    alert.show();
                    break;
                }

                this.displayPath(vertices, hamilton.getEdges());

                alert.setTitle("Hamiltonian Path");
                alert.setHeaderText("Hamiltonian Path Found");
                alert.setContentText("Path: " + hamilton.getResultString());
                alert.show();
                this.leftStatus.setText(hamilton.getExecutionTime());
                break;
            }
            case ("Find Hamiltonian Cycle"): {
                if (!this.isConnected) {
                    alert.setTitle("Hamiltonian Cycle");
                    alert.setHeaderText("No Hamiltonian Cycle Found");
                    alert.setContentText("Graph must be connected for a Hamiltonian Cycle to be Possible");
                    alert.show();
                    break;
                }
                Hamilton hamilton = new Hamilton(this.graph);
                ArrayList<Vertex> vertices = hamilton.getCycle();
                if (vertices == null) {
                    alert.setTitle("Hamiltonian Cycle");
                    alert.setHeaderText("No Hamiltonian Cycle Found");
                    alert.setContentText("No cycle was found for this graph");
                    alert.show();
                    break;
                }

                this.displayPath(vertices, hamilton.getEdges());

                alert.setTitle("Hamiltonian Cycle");
                alert.setHeaderText("Hamiltonian Cycle Found");
                alert.setContentText("Cycle: " + hamilton.getResultString());
                alert.show();
                this.leftStatus.setText(hamilton.getExecutionTime());
                break;

            }
            case ("Dijkstra's Shortest Path"): {
                this.previousState = this.cursorState;
                this.cursorState = CursorState.PATHING;
                alert.setTitle("Dijkstra");
                alert.setHeaderText("Please Specify a Path");
                alert.setContentText("Click on a Start vertex, and then on an End vertex");
                alert.show();
                break;
            }
            case ("A* Shortest Path"): {
                this.previousState = this.cursorState;
                this.cursorState = CursorState.PATHING;
                alert.setTitle("A*");
                alert.setHeaderText("Please Specify a Path");
                alert.setContentText("Click on a Start vertex, and then on an End vertex");
                alert.show();
                break;
            }
            case ("Graph Coloring"): {
                if (!this.isConnected) {
                    alert.setTitle("Graph Coloring");
                    alert.setHeaderText("No Chromatic Number Found");
                    alert.setContentText("Graph must be connected for a Chromatic Number to be calculated");
                    alert.show();
                    break;
                }

                Chromatic chromatic = new Chromatic(this.graph);
                Graph coloredGraph = chromatic.colorGraph();

                this.displayGraphColoring(coloredGraph.getVertices(), chromatic.getColorsUsed());

                alert.setTitle("Graph Coloring");
                alert.setHeaderText("Chromatic Number Found");
                alert.setContentText(chromatic.getResultString());
                alert.show();
                this.leftStatus.setText(chromatic.getExecutionTime());
                break;
            }
            case ("Travelling Salesman Tour"): {
                this.previousState = this.cursorState;
                this.cursorState = CursorState.PATHING;
                alert.setTitle("Travelling Salesman");
                alert.setHeaderText("Please Specify a Starting Point");
                alert.setContentText("Click on a Start vertex");
                alert.show();
                break;
            }
            default: {
                break;
            }
        }

    }


    /**
     * @param mouseEvent The event fired by clicking on the GUI's graphing area.
     */
    @SuppressWarnings("CodeBlock2Expr")
    public void clickedGraphPane(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            if (mouseEvent.getClickCount() == 2) {
                this.resetGraphLook();
            }
        }

        switch (this.cursorState) {
            case DEFAULT: {

                //<editor-fold desc="Making Vertex container">

                Vertex vertex = this.graph.addVertex();

                Circle circle = new Circle(15, new Color(
                        0.953, 0.953, 0.953, 1.000
                ));
                circle.setStrokeWidth(4);
                circle.setStroke(new Color(
                        1.000, 0.457, 0.418, 1.000
                ));
                Label numLabel = new Label(Integer.toString(vertex.getNumber()));
                numLabel.setStyle("-fx-font-size: 15");
                numLabel.setTextFill(new Color(
                        1.000, 0.457, 0.418, 1.000
                ));

                StackPane container = new StackPane();
                container.getChildren().addAll(circle, numLabel);
                container.setLayoutX(mouseEvent.getX() - circle.getRadius());
                container.setLayoutY(mouseEvent.getY() - circle.getRadius());
                container.setOnMouseClicked(this::clickedVertex);
                container.setOnMouseEntered((o) -> {
                    if (this.cursorState == CursorState.DIRECTED ||
                            this.cursorState == CursorState.UNDIRECTED || this.cursorState == CursorState.DELETE ||
                            this.cursorState == CursorState.MOVE || this.cursorState == CursorState.PATHING) {
                        container.setCursor(Cursor.HAND);
                    } else {
                        container.setCursor(Cursor.DEFAULT);
                    }
                });

                container.setOnMouseDragged((o) -> {
                    if (this.cursorState == CursorState.MOVE) {
                        container.setManaged(false);
                        container.setTranslateX(o.getX() + container.getTranslateX() - container.getWidth() / 2);
                        container.setTranslateY(o.getY() + container.getTranslateY() - container.getHeight() / 2);
                        o.consume();
                    }
                });

                this.splitPane.getDividers().get(0).positionProperty().addListener(((observable, oldValue, newValue) -> {
                    container.setLayoutX(container.getLayoutX() + (splitPane.getWidth() * oldValue.doubleValue() -
                            splitPane.getWidth() * newValue.doubleValue()));
                }));
                //</editor-fold>

                container.setUserData(vertex);
                container.setId("vertex" + vertex.getNumber());
                circle.setId("circle" + vertex.getNumber());
                numLabel.setId("label" + vertex.getNumber());
                this.graphPane.getChildren().add(container);
                container.toFront();

                this.leftStatus.setText("Recent Action: Added Default Vertex " + vertex.getNumber());

                break;
            }

            case HEURISTIC: {

                //<editor-fold desc="Making Heuristic Vertex container">
                String heuristicResult = this.heuristicDialog();
                if (heuristicResult == null || heuristicResult.length() < 1 || Integer.parseInt(heuristicResult) == 0) break;

                Vertex vertex = this.graph.addAStarVertex(Integer.parseInt(heuristicResult));

                Circle circle = new Circle(15, new Color(
                        0.953, 0.953, 0.953, 1.000
                ));
                circle.setStrokeWidth(4);
                circle.setStroke(new Color(
                        1.000, 0.457, 0.418, 1.000
                ));
                Label numLabel = new Label(Integer.toString(vertex.getNumber()));
                numLabel.setStyle("-fx-font-size: 15");
                numLabel.setTextFill(new Color(
                        1.000, 0.457, 0.418, 1.000
                ));

                StackPane container = new StackPane();
                container.getChildren().addAll(circle, numLabel);
                container.setLayoutX(mouseEvent.getX() - circle.getRadius());
                container.setLayoutY(mouseEvent.getY() - circle.getRadius());
                container.setOnMouseClicked(this::clickedVertex);
                container.setOnMouseEntered((o) -> {
                    if (this.cursorState == CursorState.DIRECTED ||
                            this.cursorState == CursorState.UNDIRECTED || this.cursorState == CursorState.DELETE ||
                            this.cursorState == CursorState.MOVE || this.cursorState == CursorState.PATHING) {
                        container.setCursor(Cursor.HAND);
                    } else {
                        container.setCursor(Cursor.DEFAULT);
                    }
                });

                container.setOnMouseDragged((o) -> {
                    if (this.cursorState == CursorState.MOVE) {
                        container.setManaged(false);
                        container.setTranslateX(o.getX() + container.getTranslateX() - container.getWidth() / 2);
                        container.setTranslateY(o.getY() + container.getTranslateY() - container.getHeight() / 2);
//                        o.consume();
                    }
                });

                //noinspection CodeBlock2Expr
                this.splitPane.getDividers().get(0).positionProperty().addListener(((observable, oldValue, newValue) -> {
                    container.setLayoutX(container.getLayoutX() + (splitPane.getWidth() * oldValue.doubleValue() -
                            splitPane.getWidth() * newValue.doubleValue()));
                }));

                //</editor-fold>

                container.setUserData(vertex);
                container.setId("vertex" + vertex.getNumber());
                circle.setId("circle" + vertex.getNumber());
                numLabel.setId("label" + vertex.getNumber());
                this.graphPane.getChildren().add(container);
                container.toFront();

                this.leftStatus.setText("Recent Action: Added Default Vertex " + vertex.getNumber());

                break;
            }

            default: {
                if (!this.clickedVertex) {
                    this.selectionState = SelectionState.ZERO;
                }
                this.clickedVertex = false;
            }
        }

        this.updateGraphInfo();
    }


    /**
     * @param mouseEvent The mouseEvent fired by clicking on a Vertex in the GUI's graphing pane.
     */
    public void clickedVertex(MouseEvent mouseEvent) {
        this.clickedVertex = true;
        StackPane container = (StackPane) mouseEvent.getSource();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(this.graphPane.getScene().getWindow());
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setGraphic(null);

        DialogPane pane = alert.getDialogPane();
        pane.setId("algorithmInfo");
        pane.getStylesheets().clear();
        String css = getClass().getResource("dialog.css").toExternalForm();
        pane.getStylesheets().add(css);
        pane.getStyleClass().add(css);
        if(this.cursorState != CursorState.MOVE) {
            this.resetGraphLook();
        }

        switch (this.cursorState) {
            case DELETE: {
                Vertex vertex = (Vertex) container.getUserData();

                if (vertex == null) break;

                if (this.graph.deleteVertex(vertex.getNumber())) {
                    this.removeRelevantEdges(vertex.getNumber());
                    this.graphPane.getChildren().remove(container);
                    this.leftStatus.setText("Recent Action: Deleted Vertex " + vertex.getNumber());
                }
                break;
            }

            case UNDIRECTED: {
                switch (this.selectionState) {
                    case ZERO: {
                        this.firstVertex = (Vertex) container.getUserData();
                        this.firstCircle = container;
                        this.selectionState = SelectionState.ONE;
                        break;
                    }

                    case ONE: {
                        if (container.getUserData().equals(this.firstVertex)) {
                            break;
                        }
                        this.secondVertex = (Vertex) container.getUserData();
                        this.secondCircle = container;
                        this.selectionState = SelectionState.ZERO;

                        String result = this.edgeDialog();

                        if (result == null) {
                            break;
                        } else if (result.length() != 0 && Integer.parseInt(result) == 0) {
                            this.edgeWeightErrorDialogue();
                            break;
                        } else if (result.length() == 0) {
                            result = "1";
                        }

                        Edge newEdge = new Edge(false, Integer.parseInt(result));
                        if (this.graph.addEdgeFromTo(newEdge, this.firstVertex.getNumber(), this.secondVertex.getNumber())) {

                            this.drawUndirectedEdge(newEdge, this.firstVertex.getNumber(), this.secondVertex.getNumber());
                            this.leftStatus.setText("Recent Action: Connected Vertex " + this.firstVertex.getNumber() +
                                    " to Vertex " + this.secondVertex.getNumber() + " (Undirected, Weight: " + result +
                                    ")");
                        } else {
                            this.leftStatus.setText("Recent Action: Failed to Add Edge");
                        }

                        break;
                    }
                }
                break;
            }

            case DIRECTED: {
                switch (this.selectionState) {
                    case ZERO: {
                        this.firstVertex = (Vertex) container.getUserData();
                        this.firstCircle = container;
                        this.selectionState = SelectionState.ONE;
                        break;
                    }

                    case ONE: {
                        if (container.getUserData().equals(this.firstVertex)) {
                            break;
                        }
                        this.secondVertex = (Vertex) container.getUserData();
                        this.secondCircle = container;
                        this.selectionState = SelectionState.ZERO;

                        String result = this.edgeDialog();

                        if (result == null) {
                            break;
                        } else if (result.length() != 0 && Integer.parseInt(result) == 0) {
                            this.edgeWeightErrorDialogue();
                            break;
                        } else if (result.length() == 0) {
                            result = "1";
                        }

                        Edge newEdge = new Edge(true, Integer.parseInt(result));
                        if (this.graph.addEdgeFromTo(newEdge, this.firstVertex.getNumber(), this.secondVertex.getNumber())) {

                            this.drawDirectedEdge(newEdge, this.firstVertex.getNumber(), this.secondVertex.getNumber());
                            this.leftStatus.setText("Recent Action: Connected Vertex " + this.firstVertex.getNumber() +
                                    " to Vertex " + this.secondVertex.getNumber() + " (Directed, Weight: " + result +
                                    ")");
                        } else {
                            this.leftStatus.setText("Recent Action: Failed to Add Edge");
                        }

                        break;
                    }
                }
                break;

            }
            case PATHING: {
                switch (this.selectionState) {
                    case ZERO: {
                        this.firstVertex = (Vertex) container.getUserData();
                        this.firstCircle = container;

                        if (this.algorithmComboBox.getValue().equals("Travelling Salesman Tour")) {
                            TravellingSalesman salesman = new TravellingSalesman(this.graph);
                            ArrayList<Vertex> vertices = salesman.getTourExact(this.firstVertex.getNumber());
                            if (vertices == null) {
                                alert.setTitle("Travelling Salesman");
                                alert.setHeaderText("No Tour Found");
                                alert.setContentText("No spanning cycle was found for this graph");
                                alert.show();
                                this.selectionState = SelectionState.ZERO;
                                this.cursorState = this.previousState;
                                break;
                            }

                            this.displayPath(vertices, salesman.getEdges());

                            alert.setTitle("Travelling Salesman");
                            alert.setHeaderText("Tour Found With Cost: " + salesman.getTourCost());
                            alert.setContentText("Cycle: " + salesman.getResult());
                            alert.show();
                            this.leftStatus.setText(salesman.getExecutionTime());
                            this.selectionState = SelectionState.ZERO;
                            this.cursorState = this.previousState;
                            break;
                        }
                        this.selectionState = SelectionState.ONE;
                        break;
                    }
                    case ONE: {
                        this.secondVertex = (Vertex) container.getUserData();
                        this.secondCircle = container;
                        this.selectionState = SelectionState.ZERO;
                        this.cursorState = CursorState.NONE;

                        switch(this.algorithmComboBox.getValue()) {
                            case "Dijkstra's Shortest Path": {
                                Dijkstra dijkstra = new Dijkstra(this.graph);
                                ArrayList<Vertex> vertices = dijkstra.getPathFromTo(
                                        this.firstVertex.getNumber(), this.secondVertex.getNumber());
                                if (vertices == null) {
                                    alert.setTitle("Dijkstra");
                                    alert.setHeaderText("No Path Found");
                                    alert.setContentText("No minimum path was found between the chosen vertices");
                                    alert.show();
                                    break;
                                }
                                this.cursorState = this.previousState;
                                this.displayPath(vertices, dijkstra.getEdges());

                                alert.setTitle("Dijkstra");
                                alert.setHeaderText("Path Found With Cost: " + dijkstra.getCost());
                                alert.setContentText("Path: " + dijkstra.getResult());
                                alert.show();
                                this.leftStatus.setText(dijkstra.getExecutionTime());
                                break;
                            }
                            case "A* Shortest Path": {
                                AStar aStar = new AStar(this.graph);
                                ArrayList<Vertex> vertices = aStar.getPathFromTo(
                                        this.firstVertex.getNumber(), this.secondVertex.getNumber());

                                if (vertices == null) {
                                    alert.setTitle("A*");
                                    alert.setHeaderText("No Path Found");
                                    alert.setContentText("No minimum path was found between the chosen vertices");
                                    alert.show();
                                    break;
                                }
                                this.displayPath(vertices, aStar.getEdges());
                                this.cursorState = this.previousState;

                                alert.setTitle("A*");
                                alert.setHeaderText("Path Found With Cost: " + aStar.getCost());
                                alert.setContentText("Path: " + aStar.getResult());
                                alert.show();
                                this.leftStatus.setText(aStar.getExecutionTime());

                                break;
                            }
                        }
                        break;
                    }

                }
                break;
            }
        }
        this.updateGraphInfo();
    }


    /**
     * @return A string representing the Heuristic value for a new Vertex the user
     * wishes to generate.
     */
    private String heuristicDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.initOwner(this.graphPane.getScene().getWindow());
        dialog.initStyle(StageStyle.UNDECORATED);
        dialog.setContentText("Enter a value as an integer");
        dialog.setTitle("Heuristic");
        dialog.setGraphic(null);
        dialog.setHeaderText("Provide a Heuristic For This Vertex");

        DialogPane pane = dialog.getDialogPane();
        pane.setId("heuristicDialog");
        pane.getStylesheets().clear();
        String css = getClass().getResource("dialog.css").toExternalForm();
        pane.getStylesheets().add(css);
        pane.getStyleClass().add(css);

        TextField text = dialog.getEditor();
        text.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                text.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        dialog.showAndWait();
        return dialog.getResult();
    }


    /**
     * @return A string representing the weight of a new Edge the user wishes
     * to create.
     */
    private String edgeDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.initOwner(this.graphPane.getScene().getWindow());
        dialog.initStyle(StageStyle.UNDECORATED);
        dialog.setContentText("Enter a value as an integer\n(Empty defaults to 1)");
        dialog.setTitle("Edge Weight");
        dialog.setGraphic(null);
        dialog.setHeaderText("Provide a Weight For This Edge");

        DialogPane pane = dialog.getDialogPane();
        pane.setId("edgeDialog");
        pane.getStylesheets().clear();
        String css = getClass().getResource("dialog.css").toExternalForm();
        pane.getStylesheets().add(css);
        pane.getStyleClass().add(css);

        TextField text = dialog.getEditor();
        text.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                text.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        dialog.showAndWait();
        return dialog.getResult();
    }


    /**
     *
     */
    private void edgeWeightErrorDialogue() {
        this.leftStatus.setText("Recent Action: Failed to Add Edge");

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(this.graphPane.getScene().getWindow());
        alert.initStyle(StageStyle.UNDECORATED);
        alert.setHeaderText("Invalid Entry: Edge Weight");
        alert.setContentText("Weight must be greater than 0");
        alert.setGraphic(null);

        DialogPane pane = alert.getDialogPane();
        pane.setId("edgeAlert");
        pane.getStylesheets().clear();
        String css = getClass().getResource("dialog.css").toExternalForm();
        pane.getStylesheets().add(css);
        pane.getStyleClass().add(css);

        alert.showAndWait();
    }


    /**
     * @param edge The Edge object the user wishes to draw in the GUI.
     */
    private void drawUndirectedEdge(Edge edge, int vertexNumA, int vertexNumB) {
        ArrayList<Node> delete = new ArrayList<>();
        for (Node node : this.graphPane.getChildren()) {
            if (node instanceof Line) {
                Line line = (Line) node;
                Point pointA = (Point) line.getProperties().get("pointA");
                Point pointB = (Point) line.getProperties().get("pointB");

                Point check = new Point(vertexNumA, vertexNumB);
                if ((pointA != null && Objects.equals(pointA, check)) ||
                        (pointB != null && Objects.equals(pointB, check))) {
                    Label label = (Label) line.getProperties().get("label");
                    delete.add(line);
                    delete.add(label);
                }
            } else if (node instanceof Path) {
                Path path = (Path) node;
                Point pointA = (Point) path.getProperties().get("pointA");
                Point check = new Point(vertexNumA, vertexNumB);
                Point checkB = new Point(vertexNumB, vertexNumA);
                if ((pointA != null && pointA.equals(check)) ||
                        (pointA != null && pointA.equals(checkB))) {
                    Label label = (Label) path.getProperties().get("label");
                    Circle directionCircle = (Circle) path.getProperties().get("directionCircle");
                    delete.add(path);
                    delete.add(label);
                    delete.add(directionCircle);
                }
            }
        }
        this.graphPane.getChildren().removeAll(delete);

        Line line = new Line();
        Tooltip.install(line, new Tooltip("Weight: " + edge.getWeight()));

        line.startXProperty().bind(firstCircle.layoutXProperty().add(firstCircle.translateXProperty()).add(firstCircle.widthProperty().divide(2)));
        line.startYProperty().bind(firstCircle.layoutYProperty().add(firstCircle.translateYProperty()).add(firstCircle.widthProperty().divide(2)));
        line.endXProperty().bind(secondCircle.layoutXProperty().add(secondCircle.translateXProperty()).add(secondCircle.widthProperty().divide(2)));
        line.endYProperty().bind(secondCircle.layoutYProperty().add(secondCircle.translateYProperty()).add(secondCircle.widthProperty().divide(2)));

        line.setStroke(new Color(
                1, 0.457, 0.418, 1.000
        ));

        line.setStrokeWidth(4);

        line.setOnMouseEntered((o) -> {
            if (this.cursorState == CursorState.DELETE) {
                line.setCursor(Cursor.HAND);
            } else {
                line.setCursor(Cursor.DEFAULT);
            }
        });

        Label label = new Label();
        CornerRadii cornerRadius = new CornerRadii(7);
        label.setTextAlignment(TextAlignment.CENTER);
        label.setBackground(new Background(new BackgroundFill(new Color(
                0.953, 0.953, 0.953, 1.000
        ), cornerRadius, Insets.EMPTY)));
        label.setText(Integer.toString(edge.getWeight()));
        label.setStyle("-fx-font-size: 10; -fx-text-fill: #ff756b; -fx-border-color: " + "#ff756b; -fx-border-radius: 7; -fx-border-width: 3; " +
                "-fx-label-padding: 0 5 0 5; -fx-text-alignment: center");

        label.translateXProperty().bind((line.endXProperty().add(line.startXProperty())).divide(2).subtract(label.widthProperty().divide(2)));
        label.translateYProperty().bind((line.endYProperty().add(line.startYProperty())).divide(2).subtract(label.heightProperty().divide(2)));

        Point pointA = new Point(this.firstVertex.getNumber(), this.secondVertex.getNumber());
        Point pointB = new Point(this.secondVertex.getNumber(), this.firstVertex.getNumber());

        line.setOnMouseClicked((o) -> {
            if (this.cursorState == CursorState.DELETE) {
                if (!this.graph.deleteEdgeFromTo((int) pointA.getX(), (int) pointA.getY())) {
                    try {
                        throw new Exception("EDGE NOT DELETED");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.graphPane.getChildren().removeAll(line, label);
            }
        });

        line.getProperties().put("pointA", pointA);
        line.getProperties().put("pointB", pointB);
        line.getProperties().put("edge", edge);
        line.getProperties().put("label", label);

        this.graphPane.getChildren().addAll(line, label);
        line.toBack();

    }


    /**
     * @param edge The directed edge object the user wishes to draw in the GUI.
     */
    private void drawDirectedEdge(Edge edge, int vertexNumA, int vertexNumB) {
        ArrayList<Node> delete = new ArrayList<>();
        for (Node node : this.graphPane.getChildren()) {
            if (node instanceof Path) {
                Path path = (Path) node;
                Point pointA = (Point) path.getProperties().get("pointA");
                Point check = new Point(vertexNumA, vertexNumB);
                if (pointA != null && pointA.equals(check)) {
                    Label label = (Label) path.getProperties().get("label");
                    Circle directionCircle = (Circle) path.getProperties().get("directionCircle");
                    delete.add(path);
                    delete.add(label);
                    delete.add(directionCircle);
                }
            } else if (node instanceof Line) {
                Line line = (Line) node;
                Point pointA = (Point) line.getProperties().get("pointA");
                Point pointB = (Point) line.getProperties().get("pointB");

                Point check = new Point(vertexNumA, vertexNumB);
                if ((pointA != null && Objects.equals(pointA, check)) ||
                        (pointB != null && Objects.equals(pointB, check))) {
                    Label label = (Label) line.getProperties().get("label");
                    delete.add(line);
                    delete.add(label);
                }
            }
        }
        this.graphPane.getChildren().removeAll(delete);

        Path path = new Path();
        MoveTo moveTo = new MoveTo();
        ArcTo arc = new ArcTo();
        arc.setLargeArcFlag(false);

        moveTo.xProperty().bind(firstCircle.layoutXProperty().add(firstCircle.translateXProperty()).add(firstCircle.widthProperty().divide(2)));
        moveTo.yProperty().bind(firstCircle.layoutYProperty().add(firstCircle.translateYProperty()).add(firstCircle.widthProperty().divide(2)));
        arc.xProperty().bind(secondCircle.layoutXProperty().add(secondCircle.translateXProperty()).add(secondCircle.widthProperty().divide(2)));
        arc.yProperty().bind(secondCircle.layoutYProperty().add(secondCircle.translateYProperty()).add(secondCircle.widthProperty().divide(2)));


        Label label = new Label();
        CornerRadii cornerRadius = new CornerRadii(7);
        label.setTextAlignment(TextAlignment.CENTER);
        label.setBackground(new Background(new BackgroundFill(new Color(
                0.953, 0.953, 0.953, 1.000
        ), cornerRadius, Insets.EMPTY)));
        label.setText(Integer.toString(edge.getWeight()));
        label.setStyle("-fx-font-size: 10; -fx-text-fill: #ff756b; -fx-border-color: " + "#ff756b; -fx-border-radius: 7; -fx-border-width: 3; " +
                "-fx-label-padding: 0 5 0 5; -fx-text-alignment: center");

        StackPane circle1 = this.firstCircle;
        StackPane circle2 = this.secondCircle;

        //MAKE DIRECTION CIRCLE

        Circle directionCircle = new Circle(4, new Color(
                0.953, 0.953, 0.953, 1.000
        ));
        directionCircle.setStrokeWidth(4);
        directionCircle.setStroke(new Color(
                1.000, 0.457, 0.418, 1.000
        ));


        arc.xProperty().addListener((o) -> {this.updateArcs(arc, circle1.getLayoutX() + circle1.getTranslateX(), circle1.getLayoutY() + circle1.getTranslateY(),
                circle2.getLayoutX() + circle2.getTranslateX(), circle2.getLayoutY() + circle2.getTranslateY());
            this.updateArcLabel(path, label, directionCircle);
        });

        arc.yProperty().addListener((o) -> {this.updateArcs(arc, circle1.getLayoutX() + circle1.getTranslateX(), circle1.getLayoutY() + circle1.getTranslateY(),
                circle2.getLayoutX() + circle2.getTranslateX(), circle2.getLayoutY() + circle2.getTranslateY());
            this.updateArcLabel(path, label, directionCircle);

        });

        moveTo.xProperty().addListener((o) -> {this.updateArcs(arc, circle1.getLayoutX() + circle1.getTranslateX(), circle1.getLayoutY() + circle1.getTranslateY(),
                circle2.getLayoutX() + circle2.getTranslateX(), circle2.getLayoutY() + circle2.getTranslateY());
            this.updateArcLabel(path,label, directionCircle);

        });

        moveTo.yProperty().addListener((o) -> {this.updateArcs(arc, circle1.getLayoutX() + circle1.getTranslateX(), circle1.getLayoutY() + circle1.getTranslateY(),
                circle2.getLayoutX() + circle2.getTranslateX(), circle2.getLayoutY() + circle2.getTranslateY());
            this.updateArcLabel(path, label, directionCircle);
        });

        this.updateArcs(arc, circle1.getLayoutX() + circle1.getTranslateX(), circle1.getLayoutY() + circle1.getTranslateY(),
                circle2.getLayoutX() + circle2.getTranslateX(), circle2.getLayoutY() + circle2.getTranslateY());

        path.getElements().addAll(moveTo, arc);
        path.setFill(null);
        path.setStrokeWidth(4);
        path.setStroke(new Color(1, 0.457, 0.418, 1.000));

        this.updateArcLabel(path, label, directionCircle);

        Point pointA = new Point(this.firstVertex.getNumber(), this.secondVertex.getNumber());

        path.setOnMouseClicked((o) -> {
            if (this.cursorState == CursorState.DELETE) {
                if (!this.graph.deleteEdgeFromTo((int) pointA.getX(), (int) pointA.getY())) {
                    try {
                        throw new Exception("DIRECTED EDGE NOT PROPERLY DELETED");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.graphPane.getChildren().removeAll(path, label, directionCircle);
            }
        });

        path.setOnMouseEntered((o) -> {
            if (this.cursorState == CursorState.DELETE) {
                path.setCursor(Cursor.HAND);
            } else {
                path.setCursor(Cursor.DEFAULT);
            }
        });
        Tooltip tip = new Tooltip("From " + this.firstVertex.getNumber() + " to " + this.secondVertex.getNumber());
        Tooltip.install(path, tip);

        path.getProperties().put("pointA", pointA);
        path.getProperties().put("edge", edge);
        path.getProperties().put("label", label);
        path.getProperties().put("directionCircle", directionCircle);


        this.graphPane.getChildren().addAll(path, label, directionCircle);
        label.toBack();
        directionCircle.toBack();
        path.toBack();
    }

    /**
     * Updates arc positions for directed edges.
     * @param arc The arc to be updated
     * @param startX The x position of the first vertex
     * @param startY The y position of the first vertex
     * @param endX The x position of the second vertex
     * @param endY The y position of the second vertex
     */
    private void updateArcs(ArcTo arc, double startX, double startY, double endX, double endY) {
        double hypotenuse = Math.hypot(endX - startX, endY - startY);
        double sagitta = 18;

        double radius = (Math.pow(sagitta, 2) + Math.pow(hypotenuse / 2, 2)) / (2 * sagitta);
        arc.setRadiusX(radius);
        arc.setRadiusY(radius);
    }


    /**
     *
     * @param path The path along which the label and directionCircle will be mapped
     * @param label The label whose position must be updated.
     * @param directionCircle The directionCircle whose position must be updated.
     */
    private void updateArcLabel(Path path, Label label, Circle directionCircle) {
        Circle midPoint = new Circle(2, Color.BLUE);
        PathTransition labelTransition = new PathTransition(Duration.minutes(10), path, midPoint);
        labelTransition.setOrientation(PathTransition.OrientationType.NONE);
        labelTransition.setInterpolator(Interpolator.LINEAR);
        labelTransition.jumpTo(labelTransition.getDuration().divide(2));
        labelTransition.play();

        midPoint.translateYProperty().addListener((observable, oldValue, newValue) -> {
            label.relocate(midPoint.getTranslateX() - label.getWidth()/2, midPoint.getTranslateY() - label.getHeight()/2);
            labelTransition.stop();
        });


        Circle directedPoint = new Circle(2, Color.BLUE);
        PathTransition circleTransition = new PathTransition(Duration.minutes(10), path, directionCircle);
        circleTransition.setOrientation(PathTransition.OrientationType.NONE);
        circleTransition.setInterpolator(Interpolator.LINEAR);
        circleTransition.jumpTo(Duration.minutes(8));
        circleTransition.play();

        directedPoint.translateYProperty().addListener((observable, oldValue, newValue) -> {
            directionCircle.relocate(directedPoint.getTranslateX(), directedPoint.getTranslateY());
            labelTransition.stop();
        });

    }


    /**
     * @param num The number of the node for which all
     *            connected edges must be removed from the GUI.
     */
    private void removeRelevantEdges(int num) {
        ArrayList<Node> delete = new ArrayList<>();

        for (Node node : this.graphPane.getChildren()) {
            if (node instanceof Line) {
                Line line = (Line) node;
                Point pointA = (Point) line.getProperties().get("pointA");
                Point pointB = (Point) line.getProperties().get("pointB");

                if ((int) pointA.getX() == num || (int) pointA.getY() == num || (int) pointB.getX() == num || (int) pointB.getY() == num) {
                    delete.add(line);
                    delete.add((Label) line.getProperties().get("label"));
                }
            } else if (node instanceof Path) {
                Path path = (Path) node;

                Point pointA = (Point) path.getProperties().get("pointA");

                if ((int) pointA.getX() == num || (int) pointA.getY() == num) {
                    delete.add(path);
                    delete.add((Label) path.getProperties().get("label"));
                    delete.add((Circle) path.getProperties().get("directionCircle"));
                }
            }
        }
        this.graphPane.getChildren().removeAll(delete);
    }

    /**
     *
     */
    private void updateGraphInfo() {
        this.isConnected = this.graph.isConnected();
        this.numVerticesText.setText(Integer.toString(this.graph.getVertices().size()));
        this.numEdgesText.setText(Integer.toString(this.graph.countUniqueEdges()));
        this.connectedText.setText(Boolean.toString(this.isConnected).toUpperCase());

        int strength = this.graph.getConnectionStrength();
        switch (strength) {
            case 1:
                this.strengthText.setText("Weak");
                break;
            case 2:
                this.strengthText.setText("Strong");
                break;
            default:
                this.strengthText.setText("N/A");
                break;
        }

    }

    private void resetGraphLook() {

        for (Node node : this.graphPane.getChildren()) {
            if (node instanceof Line) {
                Line line = (Line) node;
                line.setStroke(new Color(1, 0.457, 0.418, 1.000));
                ((Label) line.getProperties().get("label")).setStyle("-fx-font-size: 10; -fx-text-fill: #ff756b; -fx-border-color: " +
                        "#ff756b; -fx-border-radius: 7; -fx-border-width: 3; -fx-label-padding: 0 5 0 5; -fx-text-alignment: center");
            }
            else if (node instanceof Path) {
                Path path = (Path) node;
                path.setStroke(new Color(1, 0.457, 0.418, 1.000));

                ((Label) path.getProperties().get("label")).setStyle("-fx-font-size: 10; -fx-text-fill: #ff756b; -fx-border-color: " +
                        "#ff756b; -fx-border-radius: 7; -fx-border-width: 3; -fx-label-padding: 0 5 0 5; -fx-text-alignment: center");
                Circle directionCircle = (Circle) path.getProperties().get("directionCircle");
                directionCircle.setStroke(new Color(1, 0.457, 0.418, 1.000));
            }
        }
        for (Vertex vertex : this.graph.getVertices()) {
            Circle circle = (Circle) this.graphPane.lookup("#circle" + vertex.getNumber());
            Label label = (Label) this.graphPane.lookup("#label" + vertex.getNumber());

            circle.setStroke(new Color(1, 0.457, 0.418, 1.000));
            circle.setFill(new Color(0.953, 0.953, 0.953, 1.000));
            label.setTextFill(new Color(1, 0.457, 0.418, 1.000));

        }
    }


    private void displayMinSpanTree(Graph tree) {

        for (Vertex vertex : tree.getVertices()) {
            Circle circle = (Circle) this.graphPane.lookup("#circle" + vertex.getNumber());
            Label label = (Label) this.graphPane.lookup("#label" + vertex.getNumber());
            circle.setStroke(new Color(0.5, 0.5, 0.5, 1));
            label.setTextFill(new Color(0.5, 0.5, 0.5, 1));

        }

        for (Edge edge : tree.getEdgeMap().values()) {
            for (Node node : this.graphPane.getChildren()) {
                if (node instanceof Line) {
                    Line line = (Line) node;

                    if (line.getProperties().get("edge").equals(edge)) {
                        line.setStroke(new Color(0.5, 0.5, 0.5, 1));
                        ((Label) line.getProperties().get("label")).setStyle("-fx-font-size: 10; -fx-text-fill: #808080; -fx-border-color: " +
                                "#808080; -fx-border-radius: 7; -fx-border-width: 3; -fx-label-padding: 0 5 0 5; -fx-text-alignment: center");
                    }
                } else if (node instanceof Path) {
                    Path path = (Path) node;

                    if (path.getProperties().get("edge").equals(edge)) {
                        path.setStroke(new Color(0.5, 0.5, 0.5, 1));
                        ((Label) path.getProperties().get("label")).setStyle("-fx-font-size: 10; -fx-text-fill: #808080; -fx-border-color: " +
                                "#808080; -fx-border-radius: 7; -fx-border-width: 3; -fx-label-padding: 0 5 0 5; -fx-text-alignment: center");
                        Circle directionCircle = (Circle) path.getProperties().get("directionCircle");
                        directionCircle.setStroke(new Color(0.5, 0.5, 0.5, 1));
                    }
                }
            }

        }

    }


    private void displayGraphColoring(ArrayList<Vertex> vertices, int colorsUsed) {

        Random random = new Random();
        Color[] colors = new Color[colorsUsed];

        for (int i = 0; i < colorsUsed; i++) {
            float red = (float) (random.nextFloat() / 2f + 0.5);
            float green = (float) (random.nextFloat() / 2f + 0.5);
            float blue = (float) (random.nextFloat() / 2f + 0.5);
            Color randomColor = new Color(red, green, blue, 1);
            colors[i] = randomColor;
        }

        for (Vertex vertex : vertices) {
            Circle circle = (Circle) this.graphPane.lookup("#circle" + vertex.getNumber());
            Label label = (Label) this.graphPane.lookup("#label" + vertex.getNumber());
            circle.setStroke(new Color(0.5, 0.5, 0.5, 1));
            circle.setFill(colors[vertex.getColor()]);
            label.setTextFill(new Color(0.5, 0.5, 0.5, 1));

        }

        for (Vertex vertex : vertices) {
            vertex.setColor(-1);
        }

    }

    private void displayPath(ArrayList<Vertex> vertices, ArrayList<Edge> edges) {
        for (Vertex vertex : vertices) {
            Circle circle = (Circle) this.graphPane.lookup("#circle" + vertex.getNumber());
            Label circleLabel = (Label) this.graphPane.lookup("#label" + vertex.getNumber());

            circle.setStroke(new Color(0.5, 0.5, 0.5, 1));
            circleLabel.setTextFill(new Color(0.5, 0.5, 0.5, 1));
        }

        for (Edge edge : edges) {
            for (Node node : this.graphPane.getChildren()) {
                if (node instanceof Line) {
                    Line line = (Line) node;

                    if (line.getProperties().get("edge").equals(edge)) {
                        line.setStroke(new Color(0.5, 0.5, 0.5, 1));
                        ((Label) line.getProperties().get("label")).setStyle("-fx-font-size: 10; -fx-text-fill: #808080; -fx-border-color: " +
                                "#808080; -fx-border-radius: 7; -fx-border-width: 3; -fx-label-padding: 0 5 0 5; -fx-text-alignment: center");
                    }
                } else if (node instanceof Path) {
                    Path path = (Path) node;

                    if (path.getProperties().get("edge").equals(edge)) {
                        path.setStroke(new Color(0.5, 0.5, 0.5, 1));
                        ((Label) path.getProperties().get("label")).setStyle("-fx-font-size: 10; -fx-text-fill: #808080; -fx-border-color: " +
                                "#808080; -fx-border-radius: 7; -fx-border-width: 3; -fx-label-padding: 0 5 0 5; -fx-text-alignment: center");
                        Circle directionCircle = (Circle) path.getProperties().get("directionCircle");
                        directionCircle.setStroke(new Color(0.5, 0.5, 0.5, 1));
                    }
                }
            }

        }
    }
}
