package main.java;


import java.awt.*;
import java.util.*;

@SuppressWarnings("DuplicatedCode")
public class Graph {

    private int numDirectedEdges;
    private int connectionStrength;

    private final ArrayList<Vertex> vertices;
    private final Map<Point, Edge> edgeMap;
    private int vertexIndex;


    /**
     * The constructor for the Graph class.
     */
    public Graph() {
        this.vertices = new ArrayList<>();
        this.edgeMap = new HashMap<>(30);

        this.vertexIndex = 0;
        this.numDirectedEdges = 0;
        this.connectionStrength = 0;
    }

// --Commented out by Inspection START (5/28/20, 4:39 PM):
//    /**
//     *
//     * @return
//     */
//    public int getVertexIndex() {
//        return this.vertexIndex;
//    }
// --Commented out by Inspection STOP (5/28/20, 4:39 PM)

    /**
     * @return True if this graph contains no Vertices, false otherwise.
     */
    public boolean isEmpty() {
        return this.vertices.isEmpty();
    }

    /**
     * @return True if this graph contains any directed Edges, false otherwise.
     */
    public boolean isDirected() {
        return this.numDirectedEdges > 0;
    }

    /**
     * @param num The identifying number of the Vertex object being searched for.
     * @return The Vertex who's identifying number matches num, or null if no such Vertex exists.
     */
    public Vertex getVertexByNumber(int num) {
        if (num < 0) {
            return null;
        }

        for (Vertex vertex : this.vertices) {
            if (vertex.getNumber() == num) {
                return vertex;
            }
        }

        return null;
    }

    /**
     * @return The HashMap of Points mapping to Edges that represents all Edges
     * currently in this Graph.
     */
    public Map<Point, Edge> getEdgeMap() {
        return this.edgeMap;
    }

    /**
     * @return The ArrayList of Vertices currently contained in this Graph.
     */
    public ArrayList<Vertex> getVertices() {
        return this.vertices;
    }

    /**
     * @return The connection strength of the Graph as an int where :
     * <p>
     * 0 -> Not Connected (Directed or Undirected)
     * 1 -> Weakly Connected (Directed)
     * 2 -> Strongly Connected (Directed)
     * 3 -> Connected (Undirected)
     */
    public int getConnectionStrength() {
        return this.connectionStrength;
    }

    /**
     * @return The smallest degree for any Vertex in this graph as an int.
     */
    public int getMinDegree() {
        if (this.isEmpty()) {
            return -1;
        }
        int lowest = this.vertices.get(0).getConnections().size();

        for (Vertex vertex : this.vertices) {
            if (vertex.getConnections().size() < lowest) {
                lowest = vertex.getConnections().size();
            }
        }

        return lowest;
    }

    /**
     * @return Counts the number of unique edges in this Graph. If we add Edges with equal parameters
     * the HashTable will compare its values Collection by the Edge equals method (which I will have to alter), meaning
     * it may count too few Edges if some are similar to others. This ensures we count Edges by reference.
     */
    public int countUniqueEdges() {
        return (int) this.getEdgeMap().values().stream().distinct().count();
    }


    /**
     * @param vertexNumA The identifying number for the Vertex from which the Edge is outwards.
     * @param vertexNumB The identifying number for the Vertex for which the Edge is inwards.
     * @return The edge that leaves from a Vertex with number vertexNumA, and arrives at a Vertex with number vertexNumB.
     */
    public Edge getEdgeFromTo(int vertexNumA, int vertexNumB) {
        Point key = new Point(vertexNumA, vertexNumB);

        return this.edgeMap.get(key);
    }

    /**
     * Run a depth first search of the graph and return false if a vertex is not
     * reached. This updates the connectionStrength field of the Graph.
     *
     * @return A boolean describing whether or not this graph is connect.
     */
    public boolean isConnected() {

        /*
        Maybe incorporate identification of weakly connected vertices
         */

        if (this.vertices.isEmpty()) {
            this.connectionStrength = 0;
            return false;
        } else if (this.vertices.size() == 1) {
            this.connectionStrength = 3;
            return true;
        }


        // Depth first search of all possible paths to see that the graph is connected.
        boolean connected = true;
        boolean weakCheck = false;

        for (Vertex vertex : this.vertices) {
            ArrayList<Vertex> foundVertices = this.visitAllVertices(vertex, new ArrayList<>());
            connected = connected && (foundVertices.containsAll(this.vertices) && this.vertices.containsAll(foundVertices));
        }

        if (!connected && this.isDirected()) {

            connected = true;
            weakCheck = true;

            Graph uGraph = this.getUnderlyingGraph();

            for (Vertex vertex : uGraph.vertices) {
                ArrayList<Vertex> foundVertices = uGraph.visitAllVertices(vertex, new ArrayList<>());

                connected = connected && (foundVertices.containsAll(uGraph.vertices) &&
                        uGraph.vertices.containsAll(foundVertices));
            }
        }

        if (connected) {
            if (this.isDirected()) {
                this.connectionStrength = weakCheck ? 1 : 2;
            } else {
                this.connectionStrength = 3;
            }

        } else {
            this.connectionStrength = 0;
        }

        return connected;
    }


    /**
     * @param fromHere The vertex from which to recursively visit the rest of the
     *                 unvisited vertices in the graph.
     * @param visited An ArrayList of Vertices where each vertex included has already
     *                been visited by the Depth First Search of all the graph's Vertices.
     * @return An ArrayList of all the Vertices visited by this Depth First Search of all
     * the graph's Vertices.
     */
    public ArrayList<Vertex> visitAllVertices(Vertex fromHere, ArrayList<Vertex> visited) {
        visited.add(fromHere);

        for (Vertex nextVertex : fromHere.getConnections()) {
            if (!visited.contains(nextVertex)) {
                visited = this.visitAllVertices(nextVertex, visited);
            }
        }
        return visited;
    }

    public boolean hasCycle() {

        if((this.vertices.size() < 3 || this.countUniqueEdges() < 3) && !this.isDirected()) {
            return false;
        }

        boolean hasCycle = false;
        for (Vertex vertex : this.vertices) {
            if (hasCycle) break;

            hasCycle = this.findCycle(vertex, null, new ArrayList<>());
        }

        return hasCycle;
    }

    /**
     *
     * @param fromHere The Vertex from which to recursively search for a cycle in this graph.
     * @param visited An ArrayList of Vertices where each included Vertex has been visited by the
     *                Depth First Search of a cycle in this graph.
     * @return A boolean describing whether or not this graph contains a cycle based on its current
     * configuration of Vertices and Edges.
     */
    private boolean findCycle(Vertex fromHere, Vertex parent, ArrayList<Vertex> visited) {

        visited.add(fromHere);

        for (Vertex vertex : fromHere.getConnections()) {
            if (vertex.equals(parent)) {
                if (this.getEdgeFromTo(fromHere.getNumber(), vertex.getNumber()).isDirected()) {
                    return true;
                }else {
                    continue;
                }
            }
            if (visited.contains(vertex)) {
                return true;
            }
            if (findCycle(vertex, fromHere, visited)) {
                return true;
            }
        }
        return false;

    }

    /**
     *
     * @param edge The edge for which, upon inclusion in this graph, we wish to determine if a cycle was formed.
     * @return A boolean indicating whether adding this edge to the graph would create a cycle.
     */
    public boolean wouldCreateCycle(Edge edge, int vertexNumA, int vertexNumB) {
        if (!(this.getEdgeFromTo(vertexNumA, vertexNumB) == null) && !this.hasCycle()) {
            return false;
        }
        if (!this.addEdgeFromTo(edge, vertexNumA, vertexNumB)) {
            return false;
        }

        boolean hasCycle = this.hasCycle();
        this.deleteEdgeFromTo(vertexNumA, vertexNumB);
        return hasCycle;
    }


    /**
     * A directed graph can be strongly connected or weakly connected. To establish
     * weak connection we need to establish general connection on the underlying
     * undirected graph of the directed graph. This function produces that underlying
     * graph where all edges are undirected.
     *
     * @return The underlying graph of This Graph, where all edges are directed.
     */
    public Graph getUnderlyingGraph() {
        /*
        No need to sanitize use of method as this is a helper whose use is already guarded by isConnected()
         */

        Graph uGraph = new Graph();
        Set<Point> keySet = this.edgeMap.keySet();

        for (Vertex vertex : this.vertices) {
            Vertex newVertex = new Vertex(vertex.getNumber());
            uGraph.addVertex(newVertex);
        }
        for (Point point : keySet) {
            int weight = this.edgeMap.get(point).getWeight();

            int numA = (int) point.getX();
            int numB = (int) point.getY();

            uGraph.addEdgeFromTo(false, weight, numA, numB);
        }
        return uGraph;
    }

    /**
     * @param degree The degree for which Vertices will be searched.
     * @return An ArrayList of Vertices where each Vertex has a degree of exactly two, meaning
     * it is connected to only two edges.
     */
    public ArrayList<Vertex> getVerticesOfDegree(int degree) {
        ArrayList<Vertex> verticesOfDegree = new ArrayList<>();

        for (Vertex vertex : this.getVertices()) {
            if (vertex.getConnections().size() == degree) {
                verticesOfDegree.add(vertex);
            }
        }
        return verticesOfDegree;
    }
    /**
     * @return Returns the Vertex that was added to the graph if it was successfully added.
     */
    public Vertex addVertex() {
        Vertex newVertex = new Vertex(this.vertexIndex);
        if (this.vertices.contains(newVertex)) {
            return null;
        }

        this.vertices.add(newVertex);
        this.vertexIndex++;
        return newVertex;
    }

    /**
     * @param weight The weight of the vertex which is used in dijkstra's and A*'s
     *               path finding algorithms.
     * @return A boolean indicating whether or not the vertex was added to the graph.
     */
    public boolean addVertex(int weight) {
        Vertex newVertex = new Vertex(this.vertexIndex, weight, 0);
        if (this.vertices.contains(newVertex)) {
            return false;
        }

        this.vertices.add(newVertex);
        this.vertexIndex++;
        return true;
    }

    /**
     *
     * @param heuristic A heuristic value to be used by A* to determine the shortest path
     *                  between two nodes more efficiently than Dijkstra's algorithm.
     * @return The Vertex added to the graph, if the Vertex was successfully added.
     */
    public Vertex addAStarVertex(int heuristic) {
        Vertex newVertex = new Vertex(this.vertexIndex, Integer.MAX_VALUE, heuristic);
        if (this.vertices.contains(newVertex)) {
            return null;
        }

        this.vertices.add(newVertex);
        this.vertexIndex++;
        return newVertex;
    }

    /**
     * @param newVertex The Vertex object ot be added to this Graph.
     * @return A boolean indicating whether or not the edge was successfully added.
     */
    public boolean addVertex(Vertex newVertex) {
        if (this.vertices.contains(newVertex)) {
            return false;
        }

        this.vertices.add(newVertex);
        return true;
    }


    /**
     *
     * @param directed A boolean indicating whether or not the edge being added is directed
     * @param weight An integer describing the weight of the edge, or the cost of crossing it.
     * @param vertexNumA The number of the vertex from which the edge must exit.
     * @param vertexNumB The number of the vertex for which the path must enter.
     * @return A boolean indicating whether or not the edge was successfully added to the Graph.
     */
    public boolean addEdgeFromTo(boolean directed, int weight, int vertexNumA, int vertexNumB) {
        Edge newEdge = new Edge(directed, weight);
        if ((this.getVertexByNumber(vertexNumA) == null) || (this.getVertexByNumber(vertexNumB) == null)) {
            return false;
        }

        Point keyA = new Point(vertexNumA, vertexNumB);
        Point keyB = new Point(vertexNumB, vertexNumA);

        if (newEdge.isDirected()) {

            if (this.edgeMap.containsKey(keyB) && !this.edgeMap.get(keyB).isDirected()) {
                this.deleteEdgeFromTo(vertexNumB, vertexNumA);
            }
            this.deleteEdgeFromTo(vertexNumA, vertexNumB);
            this.edgeMap.put(keyA, newEdge);

            // Add target to connections of source
            this.getVertexByNumber(vertexNumA).addConnection(this.getVertexByNumber(vertexNumB));
            this.numDirectedEdges++;
        } else {
            this.deleteEdgeFromTo(vertexNumA, vertexNumB);
            this.deleteEdgeFromTo(vertexNumB, vertexNumA);

            this.edgeMap.put(keyA, newEdge);
            this.edgeMap.put(keyB, newEdge);

            // Add target and source to each other's connections
            this.getVertexByNumber(vertexNumA).addConnection(this.getVertexByNumber(vertexNumB));
            this.getVertexByNumber(vertexNumB).addConnection(this.getVertexByNumber(vertexNumA));
        }
        return true;
    }

    /**
     *
     * @param vertexNumA The number of the vertex from which the edge must exit.
     * @param vertexNumB The number of the vertex for which the path must enter.
     * @return A boolean indicating whether or not the edge was successfully added to the Graph.
     */
    public boolean addEdgeFromTo(Edge newEdge, int vertexNumA, int vertexNumB) {
        if ((this.getVertexByNumber(vertexNumA) == null) || (this.getVertexByNumber(vertexNumB) == null)) {
            return false;
        }

        Point keyA = new Point(vertexNumA, vertexNumB);
        Point keyB = new Point(vertexNumB, vertexNumA);

        if (newEdge.isDirected()) {

            if (this.edgeMap.containsKey(keyB) && !this.edgeMap.get(keyB).isDirected()) {
                this.deleteEdgeFromTo(vertexNumB, vertexNumA);
            }
            this.deleteEdgeFromTo(vertexNumA, vertexNumB);
            this.edgeMap.put(keyA, newEdge);

            // Add target to connections of source
            this.getVertexByNumber(vertexNumA).addConnection(this.getVertexByNumber(vertexNumB));
            this.numDirectedEdges++;
        } else {
            this.deleteEdgeFromTo(vertexNumA, vertexNumB);
            this.deleteEdgeFromTo(vertexNumB, vertexNumA);
            this.edgeMap.put(keyA, newEdge);
            this.edgeMap.put(keyB, newEdge);

            // Add target and source to each other's connections
            this.getVertexByNumber(vertexNumA).addConnection(this.getVertexByNumber(vertexNumB));
            this.getVertexByNumber(vertexNumB).addConnection(this.getVertexByNumber(vertexNumA));
        }
        return true;
    }

    /**
     * @param vertexNumber The number of the vertex to be deleted.
     * @return A boolean indicating whether or not the edge was successfully deleted from the Graph.
     */
    public boolean deleteVertex(int vertexNumber) {
        if (this.vertices.isEmpty() || this.getVertexByNumber(vertexNumber) == null || vertexNumber < 0) {
            return false;
        }

        /*
        Removes edges connected the Vertex being removed.
         */

        this.edgeMap.keySet().removeIf(point -> point.getX() == vertexNumber || point.getY() == vertexNumber);

        for (Vertex vertex : this.vertices) {
            if (vertex.getConnections().contains(this.getVertexByNumber(vertexNumber))) {
                vertex.removeConnection(this.getVertexByNumber(vertexNumber));
            }
        }

        this.vertices.remove(this.getVertexByNumber(vertexNumber));
        return true;
    }


    /**
     * @param vertexNumA The number of the vertex from which the edge must exit.
     * @param vertexNumB The number of the vertex for which the edge must enter.
     * @return A boolean indicating whether or not the edge between the two provided nodes
     * was deleted.
     */
    public boolean deleteEdgeFromTo(int vertexNumA, int vertexNumB) {
        Point keyA = new Point(vertexNumA, vertexNumB);
        Point keyB = new Point(vertexNumB, vertexNumA);

        if (!this.edgeMap.containsKey(keyA) || this.vertices.isEmpty() || (this.countUniqueEdges() == 0)) {
            return false;
        }

        Edge toRemove = this.edgeMap.get(keyA);

        this.edgeMap.remove(keyA, toRemove);
        this.edgeMap.remove(keyA);

        if (!toRemove.isDirected()) {
            this.edgeMap.remove(keyB, toRemove);
            this.edgeMap.remove(keyB);
            this.getVertexByNumber(vertexNumB).removeConnection(this.getVertexByNumber(vertexNumA));

        }
        this.getVertexByNumber(vertexNumA).removeConnection(this.getVertexByNumber(vertexNumB));
        this.numDirectedEdges--;
        return true;
    }
}