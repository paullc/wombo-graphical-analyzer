package main.java;

import java.util.ArrayList;

public class Vertex {

    private final ArrayList<Vertex> connections;
    private int color;
    private final int number;
    private int weight;
    private final int heuristic;

    /**
     * @param number The identifying number of this Node
     */
    public Vertex(int number) {
        this.connections = new ArrayList<>();
        this.number = number;
        this.weight = Integer.MAX_VALUE;
        this.color = -1;
        this.heuristic = 0;
    }

    /**
     * @param number The identifying number of this Node
     * @param weight The weight of this node if needed as a A* heuristic
     */
    public Vertex(int number, int weight, int heuristic) {
        this.connections = new ArrayList<>();
        this.number = number;
        this.weight = weight;
        this.color = -1;
        this.heuristic = heuristic;
    }

    /**
     * @return All Nodes connected to this Node as an ArrayList
     */
    public ArrayList<Vertex> getConnections() {
        return this.connections;
    }

    /**
     * @return The color number of this Node
     */
    public int getColor() {
        return this.color;
    }

    /**
     *
     * @return The Heuristic of this vertex. Used by the A* pathing algorithm to decide
     * which vertex to choose next.
     */
    public int getHeuristic() {
        return this.heuristic;
    }

    /**
     * @return The identifying number of this Node
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * @return The weight of this node if needed as a A* heuristic
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * @param weight The value to which this Node's weight will be set.
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }


    /**
     * @param color The color number of this Node
     */
    public void setColor(int color) {
        this.color = color;
    }


    /**
     * @param vertex The Node to be added as a connection to this Node
     * @return True if the Node was added, false otherwise
     */
    public boolean addConnection(Vertex vertex) {
        if (this.connections.contains(vertex)) {
            return false;
        }

        this.connections.add(vertex);
        return true;
    }

    /**
     * @param vertex The node to be removed from this Node's connections
     * @return True if the Node was removed, false otherwise
     */
    public boolean removeConnection(Vertex vertex) {
        if (!this.connections.contains(vertex)) {
            return false;
        }

        this.connections.remove(vertex);
        return true;
    }

    /**
     * @param other The object who's equality is being compared to this Node
     * @return True if the two are equal, false otherwise.
     */
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else if (this == other) {
            return true;
        } else if (this.getClass() == other.getClass()) {
            Vertex otherVertex = (Vertex) other;

            return (this.number == otherVertex.getNumber() &&
                    this.getWeight() == otherVertex.getWeight() &&
                    this.getColor() == otherVertex.getColor());
        }

        return false;
    }

}