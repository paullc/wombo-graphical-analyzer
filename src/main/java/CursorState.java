package main.java;

public enum CursorState {
    UNDIRECTED,
    DIRECTED,
    DEFAULT,
    HEURISTIC,
    DELETE,
    PATHING,
    MOVE,
    NONE
}
