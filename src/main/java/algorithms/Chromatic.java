package main.java.algorithms;

import main.java.Graph;
import main.java.Vertex;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import static java.lang.Math.pow;

/**
 *
 */
public class Chromatic {

    private final Graph graph;
    private int colorsUsed;
    private final StringBuilder result;
    private String executionTime;


    /**
     *
     * @param graph The graph on which this algorithm will be performed.
     */
    public Chromatic(Graph graph) {
        this.graph = graph.getUnderlyingGraph();
        this.result = new StringBuilder();
    }


    /**
     *
     * @return A copy of the graph used by the constructor where all edges are converted to
     * undirected edges, and all nodes have been appropriately colored.
     */
    public Graph colorGraph() {
        for (Vertex vertex : this.graph.getVertices()) {
            vertex.setColor(-1);
        }

        this.sortNodesByDegreeDec();

        ArrayList<Vertex> notColored = new ArrayList<>(this.graph.getVertices());

        long start = System.nanoTime();
        this.findChromaticNumber(notColored, 0);
        long stop = System.nanoTime();

        this.graph.getVertices().sort((nodeA, nodeB) -> {
            if(nodeA.getNumber() == nodeB.getNumber()) return 0;
            return nodeA.getNumber() < nodeB.getNumber() ? -1 : 1;
        });

        this.result.append("Chromatic Number Approximation: ").append(this.colorsUsed).append("\n");
        for(Vertex vertex : this.graph.getVertices()) {
            this.result.append("Node: ").append(vertex.getNumber()).append(" | Color Number: ").append(vertex.getColor()).append(
                    "\n");
        }
        this.executionTime = String.format("Time To Execute: %.4f milliseconds", BigDecimal.valueOf((stop - start) / (pow(10, 6))));


        return this.graph;
    }

    /**
     *
     * @param notColored The ArrayList of Vertices that have not yet been colored by the algorithms
     * @param colorToUse An integer representing a color, where this color has not yet been used by the algorithm.
     */
    private void findChromaticNumber(ArrayList<Vertex> notColored, int colorToUse) {
        ArrayList<Vertex> colored = new ArrayList<>();
        Iterator<Vertex> nodeIter = notColored.iterator();
        if (!notColored.isEmpty()) {

            while (nodeIter.hasNext()) {
                Vertex currVertex = nodeIter.next();
                boolean colorThisNode = true;

                for (Vertex checkVertex : currVertex.getConnections()) {
                    if (colored.contains(checkVertex)) {
                        colorThisNode = false;
                        break;
                    }
                }

                if (colorThisNode) {
                    currVertex.setColor(colorToUse);
                    colored.add(currVertex);
                    nodeIter.remove();
                }
            }

            this.colorsUsed++;
            this.findChromaticNumber(notColored, colorToUse + 1);
        }
    }

    /**
     *
     */
    private void sortNodesByDegreeDec() {
        this.graph.getVertices().sort((nodeA, nodeB) -> {
            if (nodeA.getConnections().size() == nodeB.getConnections().size())
                return 0;
            return nodeA.getConnections().size() > nodeB.getConnections().size() ? -1 : 1;
        });
    }

    /**
     *
     * @return A string describing the output of the algorithm if successful.
     */
    public String getResultString() {
        return this.result.toString();
    }

    /**
     *
     * @return The number of colors used to color the graph (its Chromatic number).
     */
    public int getColorsUsed(){
        return this.colorsUsed;
    }

    /**
     *
     * @return A string describing the time spent executing this algorithm, in milliseconds.
     */
    public String getExecutionTime() {
        return this.executionTime;
    }
}
