package main.java.algorithms;

import main.java.Edge;
import main.java.Graph;
import main.java.Vertex;

import java.math.BigDecimal;
import java.util.ArrayList;

import static java.lang.Math.pow;

public class Hamilton {

    private final Graph graph;
    private final ArrayList<Edge> edges;
    private boolean foundCycle;
    private boolean foundPath;
    private final StringBuilder result;
    private String executionTime;

    /**
     * @param graph The Graph for which a Hamiltonian path or cycle will be identified.
     */
    public Hamilton(Graph graph) {
        this.graph = graph;
        this.edges = new ArrayList<>();
        this.result = new StringBuilder();
    }

    /**
     * @return Returns the Graph being used by this Hamiltonian object.
     */
    public Graph getGraph() {
        return this.graph;
    }


    /**
     * @return A Hamiltonian cycle as an ArrayList of Vertices in order of required traversal, or null if there
     * exists no Hamiltonian cycle.
     */
    public ArrayList<Vertex> getCycle() {
        if (!this.graph.isConnected() ||
                this.graph.getVertices().isEmpty() ||
                (this.graph.getMinDegree() == 1 && this.graph.getVertices().size() != 2 && !this.graph.isDirected()) ||
                this.graph.getConnectionStrength() == 1) {
            return null;
        } else if (this.graph.getVertices().size() == 1) {
            return this.graph.getVertices();
        }

        Vertex startVertex = this.graph.getVertices().get(0);
        this.foundCycle = false;
        long start = System.nanoTime();
        ArrayList<Vertex> visited = this.findCycle(startVertex, startVertex, new ArrayList<>());
        long stop = System.nanoTime();

        for (int i = 0; i < visited.size() - 1; i++) {
            this.edges.add(this.graph.getEdgeFromTo(visited.get(i).getNumber(), visited.get(i + 1).getNumber()));
        }

        if (visited.containsAll(this.graph.getVertices()) && visited.size() == this.graph.getVertices().size() + 1) {
            result.append(visited.get(0).getNumber());
            for (int i = 1; i < visited.size(); i++) {
                this.result.append(" - ").append(visited.get(i).getNumber());
            }

            this.executionTime = String.format("Time To Execute: %.4f milliseconds", BigDecimal.valueOf((stop - start) / (pow(10, 6))));
            return visited;
        }

        return null;
    }


    /**
     * @return A Hamiltonian path as an ArrayList of Vertices in order of required traversal, or null if there
     * exists no Hamiltonian path.
     */
    public ArrayList<Vertex> getPath() {
        if (!this.graph.isConnected() ||
                this.graph.getVerticesOfDegree(1).size() > 2 && !this.graph.isDirected()) {
            return null;
        } else if (this.graph.getVertices().size() == 1) {
            return this.graph.getVertices();
        }

        // Order the vertices by degree.

        this.graph.getVertices().sort((o1, o2) -> {
            if (o1.getConnections().size() == o2.getConnections().size())
                return 0;
            return o1.getConnections().size() < o2.getConnections().size() ? -1 : 1;
        });


        ArrayList<Vertex> visited = new ArrayList<>();

        this.foundPath = false;
        long start = System.nanoTime();
        for (Vertex vertex : this.graph.getVertices()) {
            if (this.foundPath) {
                break;
            }
            visited = this.findPath(vertex, new ArrayList<>());
        }
        long stop = System.nanoTime();

        if (visited.containsAll(this.graph.getVertices()) && this.graph.getVertices().containsAll(visited)) {
            this.result.append(visited.get(0).getNumber());
            for (int i = 1; i < visited.size(); i++) {
                this.result.append(" - ").append(visited.get(i).getNumber());
            }
            for (int i = 0; i < visited.size() - 1; i++) {
                this.edges.add(this.graph.getEdgeFromTo(visited.get(i).getNumber(), visited.get(i + 1).getNumber()));
            }
            this.executionTime = String.format("Time To Execute: %.4f milliseconds", BigDecimal.valueOf((stop - start) / (pow(10, 6))));
            return visited;
        }

        return null;
    }

    /**
     * @param startAndEnd The Vertex where the cycle must start and finish at.
     * @param fromHere    The next Vertex in the cycle.
     * @param currentPath An ArrayList of Vertices where each Vertex, in order, is a traversed Vertex
     *                    of the current path towards a complete cycle.
     * @return The current path traveled towards a complete cycle AFTER the trying the fromHere Vertex.
     */
    private ArrayList<Vertex> findCycle(Vertex startAndEnd, Vertex fromHere, ArrayList<Vertex> currentPath) {
        currentPath.add(fromHere);
        if (fromHere == startAndEnd && currentPath.size() > 1) {
            return currentPath;
        }

        for (Vertex nextVertex : fromHere.getConnections()) {
            if (nextVertex == startAndEnd && currentPath.containsAll(this.graph.getVertices()) &&
                    this.graph.getVertices().containsAll(currentPath) && !this.foundCycle) {

                currentPath = this.findCycle(startAndEnd, nextVertex, currentPath);
                this.foundCycle = true;
                break;
            } else if (!currentPath.contains(nextVertex)) {

                currentPath = this.findCycle(startAndEnd, nextVertex, currentPath);
            }
        }

        if (!this.foundCycle) {
            currentPath.remove(fromHere);
        }
        return currentPath;
    }


    private ArrayList<Vertex> findPath(Vertex fromHere, ArrayList<Vertex> currentPath) {
        currentPath.add(fromHere);
        if (currentPath.containsAll(this.graph.getVertices()) && this.graph.getVertices().containsAll(currentPath)) {
            this.foundPath = true;
            return currentPath;
        }

        for (Vertex nextVertex : fromHere.getConnections()) {
            if (!currentPath.contains(nextVertex)) {
                currentPath = this.findPath(nextVertex, currentPath);
            }
        }

        if (!this.foundPath) {
            currentPath.remove(fromHere);
        }

        return currentPath;
    }


    /**
     *
     * @return The output of this algorithm, if successful, as a descriptive string.
     */
    public String getResultString() {
        return this.result.toString();
    }

    /**
     *
     * @return The edges included in the path or cycle found by this algorithm as an ArrayList of Edges.
     */
    public ArrayList<Edge> getEdges() {return this.edges;}

    /**
     *
     * @return A String describing the time taken to execute this algorithm.
     */
    public String getExecutionTime() {
        return this.executionTime;
    }
}
