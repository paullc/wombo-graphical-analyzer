package main.java.algorithms;

import main.java.Edge;
import main.java.Graph;
import main.java.Vertex;

import java.awt.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Set;

import static java.lang.Math.pow;

public class MinSpanTree {

    private final Graph spanningTree;
    private final Graph graph;
    private final StringBuilder result;
    private String executionTime;


    /**
     * @param graph The graph for which a minimum spanning tree is to be found.
     */
    public MinSpanTree(Graph graph) {
        this.spanningTree = new Graph();
        this.graph = graph;
        result = new StringBuilder();
    }


    /**
     * @return Returns a graph where all nodes present represent the minimum spanning tree.
     */
    public Graph getMinSpanningTree() {
        if (!this.graph.isConnected()) {
            return null;
        }
        Set<Point> keySet = this.graph.getEdgeMap().keySet();
        ArrayList<Point> points = new ArrayList<>(keySet);

        for (Vertex vertex : this.graph.getVertices()) {
            this.spanningTree.addVertex(new Vertex(vertex.getNumber()));
        }

        this.sortPoints(points);

        long start = System.nanoTime();
        for (Point point : points) {
            if (this.spanningTree.countUniqueEdges() == this.graph.getVertices().size() - 1) {
                break;
            }
            Edge toAdd = this.graph.getEdgeFromTo((int) point.getX(), (int) point.getY());
            if (!this.spanningTree.wouldCreateCycle(toAdd, (int) point.getX(), (int) point.getY())) {
                this.spanningTree.addEdgeFromTo(toAdd, (int) point.getX(), (int) point.getY());

            }
        }
        long stop = System.nanoTime();


        /*
        Sort the vertices in preparation for the StringBuilder
         */
        this.spanningTree.getVertices().sort((vertexA, vertexB) -> {
            if(vertexA.getNumber() == vertexB.getNumber()) return 0;
            return vertexA.getNumber() < vertexB.getNumber() ? -1 : 1;
        });



        this.result.append("Minimum Spanning Tree: \n");
        for (Vertex vertex : this.spanningTree.getVertices()) {
            this.result.append("Edges from Vertex ").append(vertex.getNumber()).append(" to: {");
            for (Vertex connection : vertex.getConnections()){
                this.result.append(" ").append(connection.getNumber());
                Edge edge = this.spanningTree.getEdgeFromTo(vertex.getNumber(), connection.getNumber());
                if (edge != null && edge.isDirected()) {
                    this.result.append("d");
                }
            }
            this.result.append(" }\n");
        }
        this.executionTime = String.format("Time To Execute: %.4f milliseconds", BigDecimal.valueOf((stop - start) / (pow(10, 6))));

        return this.spanningTree;

    }


    /**
     * @param points An ArrayList of Point objects that are to be sorted by edge weight.
     */
    private void sortPoints(ArrayList<Point> points) {
        points.sort((pointA, pointB) -> {
            if (this.graph.getEdgeMap().get(pointA).getWeight() ==
                    this.graph.getEdgeMap().get(pointB).getWeight())
                return 0;
            return this.graph.getEdgeMap().get(pointA).getWeight() <
                    this.graph.getEdgeMap().get(pointB).getWeight() ? -1 : 1;
        });
    }

    /**
     *
     * @return The descriptive output of this algorithm, if successful, as a String.
     */
    public String getResultString() {
        return this.result.toString();
    }


    /**
     *
     * @return The cost of crossing all the edges in the generated minimum spanning tree.
     */
    public int getCost() {
        int cost = 0;
        ArrayList<Edge> added = new ArrayList<>();
        for (Edge edge : this.spanningTree.getEdgeMap().values()) {
            if (!added.contains(edge)) {
                added.add(edge);
                cost += edge.getWeight();
            }
        }

        return cost;
    }

    /**
     *
     * @return The time required to execute this algorithm, in milliseconds.
     */
    public String getExecutionTime() {
        return this.executionTime;
    }


}
