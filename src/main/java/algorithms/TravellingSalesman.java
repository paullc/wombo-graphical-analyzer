package main.java.algorithms;

import main.java.Edge;
import main.java.Graph;
import main.java.Vertex;

import java.math.BigDecimal;
import java.util.ArrayList;

import static java.lang.Math.pow;

public class TravellingSalesman {


    private final Graph graph;
    private final ArrayList<Vertex> tour;
    private final ArrayList<Vertex> tempTour;
    private int tourCost;
    private boolean foundTour;
    private final ArrayList<Edge> edges;
    private final StringBuilder result;
    private String executionTime;

    public TravellingSalesman(Graph graph) {
        this.graph = graph;
        this.tour = new ArrayList<>();
        this.tempTour = new ArrayList<>();
        this.tourCost = Integer.MAX_VALUE;
        this.foundTour = false;
        this.edges = new ArrayList<>();
        this.result = new StringBuilder();


    }

    public ArrayList<Vertex> getTourExact(int startEnd) {
        if (!this.graph.isConnected() || graph.getConnectionStrength() == 1) return null;
        if (this.graph.getVertices().size() == 1) return this.graph.getVertices();

        Vertex startVertex = this.graph.getVertexByNumber(startEnd);
        long start = System.nanoTime();
        this.findTourExact(startVertex, startVertex);
        long stop = System.nanoTime();

        if (!this.foundTour) return null;
        if (!this.tour.containsAll(this.graph.getVertices())) return null;

        for (int i = 0; i < this.tour.size() - 1; i++) {
            this.edges.add(this.graph.getEdgeFromTo(this.tour.get(i).getNumber(), this.tour.get(i + 1).getNumber()));
        }

        for (int i = 0; i < this.tour.size(); i++) {
            if (i > 0)this.result.append(" - ");

            this.result.append(this.tour.get(i).getNumber());
        }

        this.executionTime = String.format("Time To Execute: %.4f milliseconds", BigDecimal.valueOf((stop - start) / (pow(10, 6))));
        return this.tour;
    }

    private void findTourExact(Vertex startEnd, Vertex fromHere) {
        this.tempTour.add(fromHere);
        if (fromHere == startEnd && this.tempTour.size() > 1) {
            int cost = 0;

            for (int i = 0; i < this.tempTour.size() - 1; i++) {
                Vertex vertexA = this.tempTour.get(i);
                Vertex vertexB = this.tempTour.get(i + 1);
                cost += this.graph.getEdgeFromTo(vertexA.getNumber(), vertexB.getNumber()).getWeight();
            }

            if (cost < this.tourCost) {
                this.tourCost = cost;
                this.tour.clear();
                this.tour.addAll(this.tempTour);
            }

        } else {
            for (Vertex nextVertex : fromHere.getConnections()) {
                if (nextVertex == startEnd && this.tempTour.containsAll(this.graph.getVertices()) &&
                        this.graph.getVertices().containsAll(this.tempTour)) {

                    this.findTourExact(startEnd, nextVertex);
                    this.foundTour = true;
                    break;
                } else if (!this.tempTour.contains(nextVertex)) {
                    this.findTourExact(startEnd, nextVertex);
                }
            }
        }
        this.tempTour.remove(this.tempTour.size() - 1);
    }

    public ArrayList<Edge> getEdges() {
        return this.edges;
    }

    /**
     *
     * @return The cost to complete the generated tour by crossing all its edges.
     */
    public int getTourCost() {
        return this.tourCost;
    }

    /**
     *
     * @return The descriptive output of this algorithm, if successful, as a String.
     */
    public String getResult() {
        return this.result.toString();
    }

    /**
     *
     * @return The time required to execute this algorithm as a String, in milliseconds.
     */
    public String getExecutionTime() {
        return this.executionTime;
    }
}
