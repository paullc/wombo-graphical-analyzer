package main.java.algorithms;

import main.java.Edge;
import main.java.Graph;
import main.java.Vertex;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static java.lang.Math.pow;

@SuppressWarnings("DuplicatedCode")
public class AStar {

    private final Graph graph;
    private final HashMap<Vertex, Vertex> parentVertices;
    private final ArrayList<Integer> notVisited;
    private final ArrayList<Edge> edges;
    private boolean pathFound;
    private final StringBuilder result;
    private int cost;
    private String executionTime;

    /**
     * @param graph The graph on which to perform the A* analysis
     */
    public AStar(Graph graph) {
        this.graph = graph;
        this.parentVertices = new HashMap<>();
        this.notVisited = new ArrayList<>();
        this.edges = new ArrayList<>();
        this.result = new StringBuilder();

    }

    /**
     * @param startNum The vertex number from which the path must begin
     * @param endNum The vertex number for which the path must end
     * @return The shortest path between the start and end vertices as an ArrayList of vertices in order.
     */
    public ArrayList<Vertex> getPathFromTo(int startNum, int endNum) {
        /*
        For Dijkstra's, the graph need not be connected.
         */

        this.pathFound = false;
        Vertex startVertex = this.graph.getVertexByNumber(startNum);
        Vertex endVertex = this.graph.getVertexByNumber(endNum);
        /*
        Set up A* with Vertex of distance 0. Rest should be at max int value unless otherwise
        initialized.
         */
        startVertex.setWeight(0);


        /*
        Populate notVisited
         */
        for (Vertex vertex : this.graph.getVertices()) {
            notVisited.add(vertex.getNumber());
        }

        long start = System.nanoTime();
        this.findPath(startVertex, endVertex);
        long stop = System.nanoTime();


        this.resetVertices();


        if (!this.pathFound) {
            return null;
        }

        Vertex curr = this.graph.getVertexByNumber(endNum);
        ArrayList<Vertex> path = new ArrayList<>();
        path.add(curr);
        while (!curr.equals(this.graph.getVertexByNumber(startNum))) {
            path.add(this.parentVertices.get(curr));
            curr = this.parentVertices.get(curr);
        }


        Collections.reverse(path);

        for (int i = 0; i < path.size() - 1; i++) {
            this.edges.add(this.graph.getEdgeFromTo(path.get(i).getNumber(), path.get(i + 1).getNumber()));
            this.cost += this.graph.getEdgeFromTo(path.get(i).getNumber(), path.get(i + 1).getNumber()).getWeight();
        }

        for (int i = 0; i < path.size(); i++) {
            if (i > 0) {
                this.result.append(" - ");
            }
            this.result.append(path.get(i).getNumber());
        }
        this.executionTime = String.format("Time To Execute: %.4f milliseconds", BigDecimal.valueOf((stop - start) / (pow(10, 6))));

        return path;

    }

    /**
     * @param fromHere The vertex from which to start a path
     * @param endVertex The vertex for which the path ends
     */
    private void findPath(Vertex fromHere, Vertex endVertex) {
        for (int i = 0; i < this.notVisited.size(); i++) { // Remove the Vertex we just visited from notVisited
            if (this.notVisited.get(i) == fromHere.getNumber()) {
                this.notVisited.remove(i);
                break;
            }
        }

        if (fromHere.equals(endVertex)) { // If this is the target vertex, exit.
            this.pathFound = true;
        }

        if (!this.pathFound) {
            for (Vertex vertex : fromHere.getConnections()) {
                if (this.notVisited.contains(vertex.getNumber())) {
                    int newWeight = fromHere.getWeight() + this.graph.getEdgeFromTo(fromHere.getNumber(),
                            vertex.getNumber()).getWeight();

                    if (newWeight < vertex.getWeight()) {
                        vertex.setWeight(newWeight);
                        this.parentVertices.put(vertex, fromHere);
                    }

                }
            }
        }

        Vertex nextLowest = this.getLowestDistanceVertex();

        if (!(nextLowest == null) && !this.pathFound) {
            this.findPath(nextLowest, endVertex);
        }

    }

    /**
     * @return Returns the next Vertex that will be chosen by the A* algorithm.
     *
     */
    private Vertex getLowestDistanceVertex() {

        if (this.notVisited.isEmpty()) {
            return null;
        }

        Vertex lowest = this.graph.getVertexByNumber(this.notVisited.get(0));
        for (Integer vertexNum : this.notVisited) {
            Vertex vertex = this.graph.getVertexByNumber(vertexNum);
            if (vertex.getWeight() == Integer.MAX_VALUE) {continue;}
            if ((vertex.getWeight() + vertex.getHeuristic()) < (lowest.getWeight() + lowest.getHeuristic())) {
                lowest = vertex;
            }
        }

        if (lowest.getWeight() == Integer.MAX_VALUE) {
            return null;
        }
        return lowest;
    }

    public ArrayList<Edge> getEdges() {
        return this.edges;
    }

    /**
     * Resets all vertices to default weights
     */
    private void resetVertices() {
        for (Vertex vertex : this.graph.getVertices()) {
            vertex.setWeight(Integer.MAX_VALUE);
        }
    }

    /**
     *
     * @return Returns the string result describing the successful discovery of a path.
     */
    public String getResult() {
        return this.result.toString();
    }

    /**
     *
     * @return Returns the cost of travelling the discovered path.
     */
    public int getCost() {
        return this.cost;
    }

    /**
     *
     * @return Returns the time spent executing this algorithm in milliseconds.
     */
    public String getExecutionTime() {
        return this.executionTime;
    }
}
