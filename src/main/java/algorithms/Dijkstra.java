package main.java.algorithms;

import main.java.Edge;
import main.java.Graph;
import main.java.Vertex;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static java.lang.Math.pow;

@SuppressWarnings("ALL")
public class Dijkstra {

    private final Graph graph;
    private final HashMap<Vertex, Vertex> parentVertices;
    private final ArrayList<Integer> notVisited;
    private final ArrayList<Edge> edges;
    private boolean pathFound;
    private final StringBuilder result;
    private int cost;
    private String executionTime;

    /**
     * @param graph The graph to be analyzed by this algorithms
     */
    public Dijkstra(Graph graph) {
        this.graph = graph;
        this.parentVertices = new HashMap<>();
        this.notVisited = new ArrayList<>();
        this.edges = new ArrayList<>();
        this.result = new StringBuilder();

    }

    /**
     * @param startNum The vertex number from which a valid path must begin.
     * @param endNum The vertex number for which a valid path must end.
     * @return The discovered valid path of Vertices as an ordered ArrayList. If no path is discovered
     * this will be null.
     */
    public ArrayList<Vertex> getPathFromTo(int startNum, int endNum) {
        /*
        For Dijkstra's, the graph need not be connected.
         */
        this.pathFound = false;
        Vertex startVertex = this.graph.getVertexByNumber(startNum);
        Vertex endVertex = this.graph.getVertexByNumber(endNum);
        startVertex.setWeight(0);
        /*
        Set up Dijkstra's with Vertex of distance 0. Rest should be at max int value unless otherwise
        initialized.
         */

        /*
        Populate notVisited
         */
        for (Vertex vertex : this.graph.getVertices()) {
            notVisited.add(vertex.getNumber());
        }

        long start = System.nanoTime();
        this.findPath(startVertex, endVertex);
        long stop = System.nanoTime();

        this.resetVertices();

        if (!this.pathFound) {
            return null;
        }

        Vertex curr = this.graph.getVertexByNumber(endNum);
        ArrayList<Vertex> path = new ArrayList<>();
        path.add(curr);
        while (!curr.equals(this.graph.getVertexByNumber(startNum))) {
            path.add(this.parentVertices.get(curr));
            curr = this.parentVertices.get(curr);
        }
        Collections.reverse(path);
        for (int i = 0; i < path.size() - 1; i++) {
            this.edges.add(this.graph.getEdgeFromTo(path.get(i).getNumber(), path.get(i + 1).getNumber()));
            this.cost += this.graph.getEdgeFromTo(path.get(i).getNumber(), path.get(i + 1).getNumber()).getWeight();
        }

        for (int i = 0; i < path.size(); i++) {
            if (i > 0) {
                this.result.append(" - ");
            }
            this.result.append(path.get(i).getNumber());

        }

        Collections.reverse(path);

        this.executionTime = String.format("Time To Execute: %.4f milliseconds", BigDecimal.valueOf((stop - start) / (pow(10, 6))));

        return path;
    }

    /**
     * @param fromHere The vertex from which to recursively search for the next vertices in the construction of a valid path.
     * @param endVertex The vertex for which a valid path must end.
     */
    private void findPath(Vertex fromHere, Vertex endVertex) {

        for (int i = 0; i < this.notVisited.size(); i++) { // Remove the Vertex we just visited from notVisited
            if (this.notVisited.get(i) == fromHere.getNumber()) {
                this.notVisited.remove(i);
                break;
            }
        }

        if (fromHere.equals(endVertex)) { // If this is the target vertex, exit.
            this.pathFound = true;
        }

        if (!this.pathFound) {
            for (Vertex vertex : fromHere.getConnections()) {
                if (this.notVisited.contains(vertex.getNumber())) {
                    int newWeight = fromHere.getWeight() + this.graph.getEdgeFromTo(fromHere.getNumber(),
                            vertex.getNumber()).getWeight();

                    if (newWeight < vertex.getWeight()) {
                        vertex.setWeight(newWeight);
                        this.parentVertices.put(vertex, fromHere);
                    }

                }
            }
        }

        Vertex nextLowest = this.getLowestDistanceVertex();

        if (!(nextLowest == null) && !this.pathFound) {
            this.findPath(nextLowest, endVertex);
        }

    }

    /**
     * @return The next vertex chosen by Dijkstra's algorithm during the construction of
     * a valid path.
     */
    private Vertex getLowestDistanceVertex() {

        if (this.notVisited.isEmpty()) {
            return null;
        }

        Vertex lowest = this.graph.getVertexByNumber(this.notVisited.get(0));
        for (Integer vertexNum : this.notVisited) {
            Vertex vertex = this.graph.getVertexByNumber(vertexNum);
            if (vertex.getWeight() < lowest.getWeight()) {
                lowest = vertex;
            }
        }

        if (lowest.getWeight() == Integer.MAX_VALUE) {
            return null;
        }
        return lowest;
    }


    /**
     *
     * @return All the edges included in the path returned by Dijkstra's algorithm.
     */
    public ArrayList<Edge> getEdges() {
        return this.edges;
    }

    /**
     * Resets all vertices to default weights
     */
    private void resetVertices() {
        for (Vertex vertex : this.graph.getVertices()) {
            vertex.setWeight(Integer.MAX_VALUE);
        }
    }

    /**
     *
     * @return The cost of traversing the path outputted by Dijkstra's algorithm.
     */
    public int getCost() {
        return this.cost;
    }

    /**
     *
     * @return A String describing the path constructed by Dijkstra's algorithm, if successful.
     */
    public String getResult() {
        return this.result.toString();
    }

    /**
     *
     * @return The time spent executing this algorithm as a descriptive String.
     */
    public String getExecutionTime() {
        return this.executionTime;
    }

}
