package main.java;

public class Edge {


    /**
     * Weight: Cost of travelling this edge Directed: True if edge is directed,
     * false otherwise EndNodes: part of nodes, each at one end of the edge.
     */
    private final int weight;
    private final boolean directed;


    /**
     * @param weight The weight of this edge or the cost of crossing it.
     * @param directed A boolean describing whether or not this edge is directed.
     */
    public Edge(boolean directed, int weight) {
        this.directed = directed;
        this.weight = weight;
    }

    /**
     * @return The cost of crossing the Edge as an integer
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * @return A boolean describing whether or not this edge is directed.
     */
    public boolean isDirected() {
        return this.directed;
    }

    /**
     * @param other The object that This is being compared to
     * @return A boolean describing whether or not This and other are equal.
     */
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else return this == other;
    }

}
