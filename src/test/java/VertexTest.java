package test.java;

import main.java.Vertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VertexTest {

    Vertex vertexA;
    Vertex vertexB;
    Vertex vertexC;
    Vertex vertexD;

    @BeforeEach
    public void setUp() {
        vertexA = new Vertex(0);
        vertexB = new Vertex(1);
        vertexC = new Vertex(2, 10, 0);
        vertexD = new Vertex(3, 20, 0);
    }

    @Test
    public void testGetColor() {
        assertEquals(-1, vertexA.getColor());
        assertEquals(-1, vertexB.getColor());
        assertEquals(-1, vertexC.getColor());
        assertEquals(-1, vertexD.getColor());
    }

    @Test
    public void testGetNodeNumber() {
        assertEquals(0, vertexA.getNumber());
        assertEquals(1, vertexB.getNumber());
        assertEquals(2, vertexC.getNumber());
        assertEquals(3, vertexD.getNumber());
    }

    @Test
    public void testGetWeight() {
        assertEquals(10, vertexC.getWeight());
        assertEquals(20, vertexD.getWeight());
    }

    @Test
    public void testSetColor() {
        assertEquals(-1, vertexA.getColor());
        vertexA.setColor(3);
        assertEquals(3, vertexA.getColor());
    }

    @Test
    public void testAddConnection() {
        assertEquals(0, vertexA.getConnections().size());
        assertTrue(vertexA.addConnection(vertexB));
        assertEquals(1, vertexA.getConnections().size());
        assertFalse(vertexA.addConnection(vertexB));

        Vertex copyVertexB = new Vertex(1);

        assertFalse(vertexA.addConnection(copyVertexB));
        assertEquals(1, vertexA.getConnections().size());
    }

    @Test
    public void testRemoveConnection() {
        assertEquals(0, vertexA.getConnections().size());
        assertTrue(vertexA.addConnection(vertexB));
        assertEquals(1, vertexA.getConnections().size());
        assertFalse(vertexA.addConnection(vertexB));

        assertTrue(vertexA.removeConnection(vertexB));
        assertEquals(0, vertexA.getConnections().size());

    }

    @Test
    public void testGetConnection() {
        assertNotNull(vertexA.getConnections());
        assertEquals(0, vertexA.getConnections().size());
        assertTrue(vertexA.addConnection(vertexB));
        assertEquals(1, vertexA.getConnections().size());
        assertFalse(vertexA.addConnection(vertexB));

        Vertex copyVertexB = new Vertex(1);

        assertNotNull(copyVertexB.getConnections());
        assertEquals(0, copyVertexB.getConnections().size());

        assertFalse(vertexA.addConnection(copyVertexB));
        assertEquals(1, vertexA.getConnections().size());
    }

    @Test
    public void testEquals() {
        assertNotEquals(vertexA, vertexB);
        assertEquals(vertexA, vertexA);

        Vertex copyVertexA = new Vertex(0);

        assertEquals(vertexA, copyVertexA);

        Vertex copyVertexD = new Vertex(3, 20, 0);
        assertEquals(vertexD, copyVertexD);
    }
}
