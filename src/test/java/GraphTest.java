package test.java;

import main.java.Edge;
import main.java.Graph;
import main.java.Vertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;


public class GraphTest {

    Graph graphA;
    Vertex vertexA;
    Vertex vertexB;
    Vertex vertexC;

    @BeforeEach
    public void setUp() {

        graphA = new Graph();
        vertexA = new Vertex(0);
        vertexB = new Vertex(1);
        vertexC = new Vertex(2);

    }

    @Test
    public void testIsEmpty() {

        assertTrue(graphA.isEmpty());
        assertTrue(graphA.addVertex(vertexA));
        assertFalse(graphA.isEmpty());
    }

    @Test
    public void testAddVertex() {

        assertTrue(graphA.isEmpty());

        assertTrue(graphA.addVertex(vertexA));
        assertTrue(graphA.addVertex(vertexB));
        /*
        Should not add repeated node
         */
        assertFalse(graphA.addVertex(vertexA));

        assertFalse(graphA.isEmpty());

        assertEquals(vertexA, graphA.getVertexByNumber(vertexA.getNumber()));
        assertEquals(vertexB, graphA.getVertexByNumber(vertexB.getNumber()));
    }

    @Test
    public void testDeleteVertex() {
        assertTrue(graphA.isEmpty());

        assertTrue(graphA.addVertex(vertexA));
        assertTrue(graphA.addVertex(vertexB));

        assertFalse(graphA.isEmpty());
        assertEquals(2, graphA.getVertices().size());
        assertTrue(graphA.getVertices().contains(vertexA));
        assertTrue(graphA.getVertices().contains(vertexB));

        assertTrue(graphA.deleteVertex(0));
        assertFalse(graphA.getVertices().contains(vertexA));
        assertTrue(graphA.getVertices().contains(vertexB));
        assertEquals(1, graphA.getVertices().size());
        assertFalse(graphA.deleteVertex(4));
        assertEquals(1, graphA.getVertices().size());


    }


    @Test
    public void testGetVertexByNumber() {

        assertTrue(graphA.isEmpty());
        assertTrue(graphA.addVertex(vertexA));
        assertTrue(graphA.addVertex(vertexB));
        assertFalse(graphA.isEmpty());

        assertEquals(vertexA, graphA.getVertexByNumber(0));
        assertEquals(vertexB, graphA.getVertexByNumber(1));
    }

    @Test
    public void testGetEdgeMap() {

        // EMPTY EDGE MAP
        assertTrue(graphA.isEmpty());

        assertTrue(graphA.getEdgeMap().isEmpty());

        assertTrue(graphA.addVertex(vertexA));
        assertTrue(graphA.addVertex(vertexB));
        assertTrue(graphA.addVertex(vertexC));

        assertTrue(graphA.getEdgeMap().isEmpty());
        assertFalse(graphA.getVertices().isEmpty());

        // ADD AN UNDIRECTED EDGE BETWEEN NODE A AND NODE B

        graphA.addEdgeFromTo(false, 10, 0, 1);

        assertFalse(graphA.getEdgeMap().isEmpty());
        assertFalse(graphA.getEdgeMap().get(new Point(0, 1)).isDirected());
        assertEquals(10, graphA.getEdgeMap().get(new Point(0, 1)).getWeight());

        // Undirected edges adds two mappings.
        assertEquals(1, graphA.countUniqueEdges());

        graphA.addEdgeFromTo(true, 15, 0, 2);

        assertFalse(graphA.getEdgeMap().isEmpty());
        assertTrue(graphA.getEdgeMap().get(new Point(0, 2)).isDirected());
        assertEquals(15, graphA.getEdgeMap().get(new Point(0, 2)).getWeight());

        // One directed edge, one undirected edge = 3 mappings
        assertEquals(2, graphA.countUniqueEdges());
    }

    @Test
    public void testAddEdgeFromTo() {
        assertTrue(graphA.getEdgeMap().isEmpty());

        assertFalse(graphA.addEdgeFromTo(false, 10, 0, 1));

        assertTrue(graphA.addVertex(vertexA));
        assertTrue(graphA.addVertex(vertexB));

        assertEquals(0, graphA.countUniqueEdges());

        // WE ADD EDGE A TO GRAPH A BETWEEN NODES A AND B
        assertTrue(graphA.addEdgeFromTo(false, 10, 0, 1));
        assertEquals(1, graphA.countUniqueEdges());

        // EDGE B THEN REPLACES THE EDGE WE ORIGINALLY PLACED BETWEEN NODE A AND B

        assertTrue(graphA.addEdgeFromTo(false, 20, 0, 1));
        assertEquals(1, graphA.countUniqueEdges());

        /*
        Now test adding directed edge to an edge that is undirected
         */
        assertTrue(graphA.addEdgeFromTo(true, 20, 0, 1));
        assertEquals(1, graphA.countUniqueEdges());
    }

    @Test
    public void testAddEdgeFromTo2() {
        assertTrue(graphA.getEdgeMap().isEmpty());

        assertFalse(graphA.addEdgeFromTo(false, 10, 0, 1));

        assertNotNull(graphA.addVertex());
        assertNotNull(graphA.addVertex());

        assertEquals(0, graphA.countUniqueEdges());

        assertTrue(graphA.addEdgeFromTo(false, 10, 0, 1));
        assertEquals(1, graphA.countUniqueEdges());

        assertTrue(graphA.addEdgeFromTo(false, 10, 0, 1));
        assertEquals(1, graphA.countUniqueEdges());


        assertTrue(graphA.addEdgeFromTo(false, 10, 1, 0));
        assertEquals(1, graphA.countUniqueEdges());

        assertTrue(graphA.addEdgeFromTo(false, 10, 0, 1));
        assertEquals(1, graphA.countUniqueEdges());



    }

    @Test
    public void testDeleteEdgeFromTo() {
        assertTrue(graphA.getEdgeMap().isEmpty());

        assertFalse(graphA.deleteEdgeFromTo(2, 4));

        assertTrue(graphA.addVertex(vertexA));
        assertTrue(graphA.addVertex(vertexB));
        assertEquals(0, graphA.countUniqueEdges());

        // WE ADD EDGE A TO GRAPH A BETWEEN NODES A AND B
        assertTrue(graphA.addEdgeFromTo(false, 10, 0, 1));
        assertEquals(1, graphA.countUniqueEdges());

        assertTrue(graphA.deleteEdgeFromTo(0, 1));
        assertNull(graphA.getEdgeFromTo(0, 1));
        assertEquals(0, graphA.countUniqueEdges());


        /*
        Test removing directed edges.
         */

        assertTrue(graphA.addEdgeFromTo(true, 10, 0, 1));
        assertTrue(graphA.addEdgeFromTo(true, 10, 1, 0));
        assertFalse(graphA.deleteEdgeFromTo(2, 4));
        assertEquals(2, graphA.countUniqueEdges());
        assertTrue(graphA.deleteEdgeFromTo(0, 1));
        assertEquals(1, graphA.countUniqueEdges());


    }

    @Test
    public void testCountUniqueEdges() {
        assertTrue(graphA.addVertex(vertexA));
        assertTrue(graphA.addVertex(vertexB));

        assertEquals(0, graphA.countUniqueEdges());
        graphA.addEdgeFromTo(false, 10, 0, 1);

        // An undirected edge results in two mappings, but one distinct edge.
        assertEquals(2, graphA.getEdgeMap().values().size());
        assertEquals(1, graphA.countUniqueEdges());
    }

    @Test
    public void testGetEdgeFromTo() {
        assertTrue(graphA.addVertex(0));
        assertTrue(graphA.addVertex(0));
        assertTrue(graphA.addVertex(0));

        assertEquals(0, graphA.countUniqueEdges());
        graphA.addEdgeFromTo(false, 10, 0, 1);
        graphA.addEdgeFromTo(true, 15, 1, 2);
        graphA.addEdgeFromTo(true, 15, 2, 0);
        assertEquals(3, graphA.countUniqueEdges());

        // UNDIRECTED EDGE CHECK
        assertEquals(10, graphA.getEdgeFromTo(0, 1).getWeight());
        assertEquals(10, graphA.getEdgeFromTo(1, 0).getWeight());
        assertFalse(graphA.getEdgeFromTo(1, 0).isDirected());

        // DIRECTED EDGE CHECK
        assertEquals(15, graphA.getEdgeFromTo(1, 2).getWeight());
        assertNotEquals(graphA.getEdgeFromTo(1, 2), graphA.getEdgeFromTo(2, 1));
        //could cause issues, remove if necessary.
    }

    @Test
    public void testIsConnected() {

        // ZERO NODES - DISCONNECTED
        assertFalse(graphA.isConnected());
        assertEquals(0, graphA.getConnectionStrength());

        // ONE SOLITARY NODE - NO EDGES - CONNECTED
        // O
        assertTrue(graphA.addVertex(0));
        assertTrue(graphA.isConnected());
        assertEquals(3, graphA.getConnectionStrength());

        // TWO NODES - NO EDGES - DISCONNECTED
        // O      O
        assertTrue(graphA.addVertex(0));
        assertFalse(graphA.isConnected());
        assertEquals(0, graphA.getConnectionStrength());

        // TWO NODES - ONE EDGE - CONNECTED
        // O ----- O
        assertTrue(graphA.addEdgeFromTo(false, 5, 0, 1));
        assertTrue(graphA.isConnected());
        assertEquals(3, graphA.getConnectionStrength());

        // FIVE NODES - THREE EDGES - DISCONNECTED

        //  O ----- O ----- O ----- O       O

        // We have to make new edges because we never add the same exact Edge reference
        // to the Graph.
        assertTrue(graphA.addVertex(0));
        assertTrue(graphA.addVertex(0));
        assertTrue(graphA.addVertex(0));
        assertTrue(graphA.addEdgeFromTo(false, 5, 1, 2));
        assertTrue(graphA.addEdgeFromTo(false, 5, 2, 3));
        assertFalse(graphA.isConnected());
        assertEquals(5, graphA.getVertices().size());
        assertEquals(3, graphA.countUniqueEdges());

        // FIVE NODES - FOUR EDGES - WEAKLY CONNECTED
        //  |----------<------<---------<----
        //  |                               ^
        //  O ----- O ----- O ----- O       O
        assertTrue(graphA.addEdgeFromTo(true, 10, 4, 0));
        assertTrue(graphA.isConnected());
        assertEquals(1, graphA.getConnectionStrength());


        // FIVE NODES - FIVE EDGES - STRONGLY CONNECTED
        //  |----------<------<---------<----
        //  |                               ^
        //  O ----- O ----- O ----- O ----> O
        assertTrue(graphA.addEdgeFromTo(true, 10, 3, 4));
        assertTrue(graphA.isConnected());
        assertEquals(2, graphA.getConnectionStrength());
    }

    @Test
    public void testHasCycle() {
        this.graphA.addVertex();
        assertFalse(this.graphA.hasCycle());
        this.graphA.addVertex();
        assertFalse(this.graphA.hasCycle());

        this.graphA.addEdgeFromTo(false, 5, 0, 1);
        assertFalse(this.graphA.hasCycle());

        this.graphA.addVertex();
        assertFalse(this.graphA.hasCycle());

        this.graphA.addEdgeFromTo(false, 10, 1, 2);
        assertFalse(this.graphA.hasCycle());
        this.graphA.addEdgeFromTo(false, 15, 0, 2);
        assertTrue(this.graphA.hasCycle());

    }

    @Test
    public void testHasCycle2() {
        this.graphA.addVertex();
        this.graphA.addVertex();
        this.graphA.addVertex();
        this.graphA.addVertex();
        this.graphA.addVertex();
        this.graphA.addVertex();

        assertFalse(this.graphA.hasCycle());

        this.graphA.addEdgeFromTo(false, 5, 0, 1);
        this.graphA.addEdgeFromTo(false, 10, 1, 4);
        this.graphA.addEdgeFromTo(false, 15, 4, 3);
        assertFalse(this.graphA.hasCycle());


    }

    @Test
    public void testWouldCreateCycle() {

        this.graphA.addVertex();

        assertFalse(this.graphA.hasCycle());

        this.graphA.addVertex();

        assertFalse(this.graphA.hasCycle());
        assertFalse(graphA.wouldCreateCycle(new Edge(false, 5), 0, 1));

        this.graphA.addVertex();
        this.graphA.addEdgeFromTo(false, 5, 0, 1);

        assertFalse(this.graphA.hasCycle());
        assertFalse(graphA.wouldCreateCycle(new Edge(false, 5), 1, 2));

        this.graphA.addEdgeFromTo(false, 5, 1, 2);
        assertFalse(this.graphA.hasCycle());
        assertTrue(graphA.wouldCreateCycle(new Edge(false, 5), 0, 2));




    }

}
