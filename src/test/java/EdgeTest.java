package test.java;


import main.java.Edge;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EdgeTest {

    Edge edgeA;
    Edge copyEdgeA;
    Edge edgeB;

    @BeforeEach
    public void setUp() {

        edgeA = new Edge(true, 5);
        copyEdgeA = new Edge(true, 5);
        edgeB = new Edge(false, 10);
    }

    @Test
    public void testGetWeight() {
        assertEquals(5, edgeA.getWeight());
        assertEquals(5, copyEdgeA.getWeight());
        assertEquals(10, edgeB.getWeight());

    }

    @Test
    public void testIsDirected() {
        assertTrue(edgeA.isDirected());
        assertTrue(copyEdgeA.isDirected());
        assertFalse(edgeB.isDirected());
    }

    @Test
    public void testEquals() {
        assertEquals(edgeA, edgeA);
        assertNotEquals(edgeA, copyEdgeA);
        assertNotEquals(edgeA, edgeB);
    }
}