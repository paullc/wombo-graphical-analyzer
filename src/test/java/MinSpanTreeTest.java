package test.java;

import main.java.Graph;
import main.java.algorithms.MinSpanTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MinSpanTreeTest {

    private Graph graph;
    private MinSpanTree tree;

    @BeforeEach
    public void setUp() {
        this.graph = new Graph();
        this.tree = new MinSpanTree(this.graph);
    }


    @Test
    public void testGetMinSpanningTree() {

        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();

        this.graph.addEdgeFromTo(false, 1, 0, 1);
        this.graph.addEdgeFromTo(false, 3, 0, 3);

        this.graph.addEdgeFromTo(false, 5, 1, 3);
        this.graph.addEdgeFromTo(false, 1, 1, 4);
        this.graph.addEdgeFromTo(false, 6, 1, 2);

        this.graph.addEdgeFromTo(false, 5, 2, 4);
        this.graph.addEdgeFromTo(false, 2, 2, 5);

        this.graph.addEdgeFromTo(false, 1, 3, 4);

        this.graph.addEdgeFromTo(false, 4, 4, 5);


        assertNotNull(this.tree.getMinSpanningTree());




    }
}
