package test.java;

import main.java.Graph;
import main.java.algorithms.TravellingSalesman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TravellingSalesmanTest {

    private Graph graph;
    private TravellingSalesman salesman;

    @BeforeEach
    public void setUp() {

        this.graph = new Graph();
        this.salesman = new TravellingSalesman(this.graph);
    }

    @Test
    public void testGetTourExact() {

        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());

        assertTrue(this.graph.addEdgeFromTo(false, 12,0, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 19,0, 3));
        assertTrue(this.graph.addEdgeFromTo(false, 8,0, 2));
        assertTrue(this.graph.addEdgeFromTo(false, 10,0, 1));

        assertTrue(this.graph.addEdgeFromTo(false, 3,1, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 6,1, 3));
        assertTrue(this.graph.addEdgeFromTo(false, 20,1, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 2,2, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 4,2, 3));

        assertTrue(this.graph.addEdgeFromTo(false, 7,3, 4));

        assertNotNull(this.salesman.getTourExact(4));


    }
}
