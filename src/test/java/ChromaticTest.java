package test.java;

import main.java.Graph;
import main.java.algorithms.Chromatic;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("DuplicatedCode")
public class ChromaticTest {

    private Graph graph;
    private Chromatic chromatic;

    @BeforeEach
    public void setUp() {

        this.graph = new Graph();
        this.chromatic = new Chromatic(this.graph);
    }


    @Test
    public void testGetChromaticNumber() {

        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();


        this.graph.addEdgeFromTo(false, 5, 0, 2);
        this.graph.addEdgeFromTo(false, 5, 0, 1);

        this.graph.addEdgeFromTo(false, 5, 1, 2);
        this.graph.addEdgeFromTo(false, 5, 1, 3);
        this.graph.addEdgeFromTo(false, 5, 1, 4);

        this.graph.addEdgeFromTo(false, 5, 2, 3);
        this.graph.addEdgeFromTo(false, 5, 2, 4);
        this.chromatic = new Chromatic(this.graph);

        this.chromatic.colorGraph();
        assertEquals(3, chromatic.getColorsUsed());

    }

    @Test
    public void testGetChromaticNumber2() {

        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();
        this.graph.addVertex();


        this.graph.addEdgeFromTo(false, 5, 0, 2);
        this.graph.addEdgeFromTo(false, 5, 0, 1);
        this.graph.addEdgeFromTo(false, 5, 0, 3);


        this.graph.addEdgeFromTo(false, 5, 1, 2);
        this.graph.addEdgeFromTo(false, 5, 1, 3);
        this.graph.addEdgeFromTo(false, 5, 1, 4);

        this.graph.addEdgeFromTo(false, 5, 2, 3);
        this.graph.addEdgeFromTo(false, 5, 2, 4);
        this.chromatic = new Chromatic(this.graph);


        this.chromatic.colorGraph();
        assertEquals(4, chromatic.getColorsUsed());

    }
}
