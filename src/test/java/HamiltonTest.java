package test.java;

import main.java.Graph;
import main.java.algorithms.Hamilton;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("DuplicatedCode")
public class HamiltonTest {

    private Graph graph;
    private Hamilton hamilton;

    @BeforeEach
    public void setUp() {
        this.graph = new Graph();
        this.hamilton = new Hamilton(graph);

    }

    @Test
    public void testGetGraph() {
        assertEquals(this.graph, this.hamilton.getGraph());

        assertTrue(this.graph.addVertex(0));
        assertTrue(this.graph.addVertex(0));

        assertEquals(this.graph, this.hamilton.getGraph());
        assertEquals(2, this.hamilton.getGraph().getVertices().size());
    }

    @Test
    public void testVerticesOfDegree() {
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertEquals(0, this.hamilton.getGraph().getVerticesOfDegree(2).size());

        assertTrue(this.graph.addEdgeFromTo(false, 5, 0, 1));
        assertEquals(0, this.hamilton.getGraph().getVerticesOfDegree(2).size());

        assertTrue(this.graph.addEdgeFromTo(false, 10, 1, 2));
        assertEquals(1, this.hamilton.getGraph().getVerticesOfDegree(2).size());

        assertTrue(this.graph.addEdgeFromTo(false, 15, 2, 0));
        assertEquals(3, this.hamilton.getGraph().getVerticesOfDegree(2).size());
    }

    @Test
    public void testGetCycle() {
        assertTrue(graph.isEmpty());
        assertEquals(0, graph.getVertices().size());
        assertEquals(0, graph.countUniqueEdges());

        assertNull(this.hamilton.getCycle());
        assertTrue(graph.addVertex(0));
        assertNotNull(this.hamilton.getCycle());
        assertEquals(this.graph.getVertices(), this.hamilton.getCycle());
        assertEquals(this.hamilton.getGraph().getVertexByNumber(0), this.hamilton.getCycle().get(0));


        assertTrue(graph.addVertex(0));
        assertNull(this.hamilton.getCycle());
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 1));

        assertNotNull(this.hamilton.getCycle());
        assertEquals(3, this.hamilton.getCycle().size());
        assertEquals(this.hamilton.getGraph().getVertexByNumber(0), this.hamilton.getCycle().get(0));
        assertEquals(this.hamilton.getGraph().getVertexByNumber(1), this.hamilton.getCycle().get(1));
        assertEquals(this.hamilton.getGraph().getVertexByNumber(0), this.hamilton.getCycle().get(2));

        assertTrue(graph.addVertex(0));

        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 2));
        assertNull(this.hamilton.getCycle());

        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 2));
        assertNotNull(this.hamilton.getCycle());
        assertEquals(4, this.hamilton.getCycle().size());
    }

    @Test
    public void testGetCycleComplex() {

        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));

        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 1));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 2));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 6));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 3));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,2, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 6));

        assertNotNull(this.hamilton.getCycle());

    }

    @Test
    public void testGetCycleComplex2() {

        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));


        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 6));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 6));

        assertTrue(this.graph.addEdgeFromTo(false, 5,7, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,7, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 5,5, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 2));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 5));

        assertNotNull(this.hamilton.getCycle());
    }
    @Test
    public void testGetCycleComplex3() {
        //7 Vertices, 13 Edges

        assertNotNull(graph.addVertex());
        assertTrue(graph.deleteVertex(0));
        assertNotNull(graph.addVertex());
        assertNotNull(graph.addVertex());
        assertNotNull(graph.addVertex());
        assertNotNull(graph.addVertex());
        assertNotNull(graph.addVertex());
        assertNotNull(graph.addVertex());


        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 2));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 3));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 6));

        assertTrue(this.graph.addEdgeFromTo(false, 5,2, 3));
        assertTrue(this.graph.addEdgeFromTo(false, 5,2, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,2, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 6));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 4));

        assertTrue(this.graph.addEdgeFromTo(false, 5,4, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 5,5, 6));


        assertNotNull(this.hamilton.getCycle());
    }

    @Test
    public void testGetPathComplex() {

        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));


        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 6));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 6));

        assertTrue(this.graph.addEdgeFromTo(false, 5,7, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,7, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 5,5, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 2));

        assertNotNull(this.hamilton.getPath());

    }

    @Test
    public void testGetPathComplex2() {

        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));
        assertTrue(graph.addVertex(0));


        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 6));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 6));

        assertTrue(this.graph.addEdgeFromTo(false, 5,7, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,7, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 5,5, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,3, 2));

        assertTrue(this.graph.addEdgeFromTo(true, 5,1, 8));

        assertNotNull(this.hamilton.getPath());

    }
}
