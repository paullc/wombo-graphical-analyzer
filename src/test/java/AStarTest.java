package test.java;

import main.java.Graph;
import main.java.algorithms.AStar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings("DuplicatedCode")
public class AStarTest {

    private AStar aStar;
    private Graph graph;


    @BeforeEach
    public void setUp() {
        this.graph = new Graph();
        this.aStar = new AStar(this.graph);
    }

    @Test
    public void testGetPathFromTo() {
        assertNotNull(this.graph.addAStarVertex(14));
        assertNotNull(this.graph.addAStarVertex(12));
        assertNotNull(this.graph.addAStarVertex(11));
        assertNotNull(this.graph.addAStarVertex(6));
        assertNotNull(this.graph.addAStarVertex(4));
        assertNotNull(this.graph.addAStarVertex(11));
        assertNotNull(this.graph.addAStarVertex(0));

        assertTrue(this.graph.addEdgeFromTo(false, 4,0, 1));
        assertTrue(this.graph.addEdgeFromTo(false, 3,0, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 12,1, 4));


        assertTrue(this.graph.addEdgeFromTo(false, 10,2, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 7,2, 3));

        assertTrue(this.graph.addEdgeFromTo(false, 2,3, 4));

        assertTrue(this.graph.addEdgeFromTo(false, 16,6, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,6, 4));

        assertNotNull(this.aStar.getPathFromTo(0, 6));
    }

    @Test
    public void testGetPathFromTo2() {
        assertNotNull(this.graph.addAStarVertex(21));
        assertNotNull(this.graph.addAStarVertex(14));
        assertNotNull(this.graph.addAStarVertex(18));
        assertNotNull(this.graph.addAStarVertex(18));
        assertNotNull(this.graph.addAStarVertex(5));
        assertNotNull(this.graph.addAStarVertex(8));
        assertNotNull(this.graph.addAStarVertex(2));

        assertTrue(this.graph.addEdgeFromTo(false, 9,0, 1));
        assertTrue(this.graph.addEdgeFromTo(false, 4,0, 2));
        assertTrue(this.graph.addEdgeFromTo(false, 7,0, 3));

        assertTrue(this.graph.addEdgeFromTo(false, 11,1, 4));


        assertTrue(this.graph.addEdgeFromTo(false, 17,2, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 12,2, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 14,3, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 5,4, 6));

        assertTrue(this.graph.addEdgeFromTo(false, 9,5, 6));

        assertNotNull(this.aStar.getPathFromTo(0, 6));

    }

}
