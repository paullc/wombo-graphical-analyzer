package test.java;

import main.java.Graph;
import main.java.Vertex;
import main.java.algorithms.Dijkstra;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("DuplicatedCode")
public class DijkstraTest {

    private Graph graph;
    private Dijkstra dijkstra;

    @BeforeEach
    public void setUp() {
        this.graph = new Graph();
        this.dijkstra = new Dijkstra(this.graph);
    }

    @Test
    public void testGetPathFromTo() {

        assertNotNull(graph.addVertex());

        assertNotNull(graph.addVertex());

        assertNotNull(graph.addVertex());

        assertNotNull(graph.addVertex());

        assertNotNull(graph.addVertex());

        assertNotNull(graph.addVertex());

        assertNotNull(graph.addVertex());

        assertNotNull(graph.addVertex());



        assertTrue(this.graph.addEdgeFromTo(false, 2,0, 6));
        assertTrue(this.graph.addEdgeFromTo(false, 3,0, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 2,0, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 5,0, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 2,1, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 4,1, 6));

        assertTrue(this.graph.addEdgeFromTo(false, 3,7, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 2,7, 5));

        assertTrue(this.graph.addEdgeFromTo(false, 4,5, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 2,3, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 2,3, 2));

        ArrayList<Vertex> path = this.dijkstra.getPathFromTo(6, 3);

        assertNotNull(path);
        assertEquals(this.graph.getVertexByNumber(6), path.get(0));
        assertEquals(this.graph.getVertexByNumber(0), path.get(1));
        assertEquals(this.graph.getVertexByNumber(4), path.get(2));
        assertEquals(this.graph.getVertexByNumber(3), path.get(3));

    }

    @Test
    public void testGetPathFromTo2() {

        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());
        assertNotNull(this.graph.addVertex());

        assertTrue(this.graph.addEdgeFromTo(false, 4,0, 1));
        assertTrue(this.graph.addEdgeFromTo(false, 3,0, 2));

        assertTrue(this.graph.addEdgeFromTo(false, 5,1, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 12,1, 4));


        assertTrue(this.graph.addEdgeFromTo(false, 10,2, 4));
        assertTrue(this.graph.addEdgeFromTo(false, 7,2, 3));

        assertTrue(this.graph.addEdgeFromTo(false, 2,3, 4));

        assertTrue(this.graph.addEdgeFromTo(false, 16,6, 5));
        assertTrue(this.graph.addEdgeFromTo(false, 5,6, 4));

        assertNotNull(this.dijkstra.getPathFromTo(0, 6));
    }
}
